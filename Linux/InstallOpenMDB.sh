# Programa para abrir os arquivos *.mdb
# http://www.vivaolinux.com.br/artigo/Utilizando-arquivos-Access-%28.mdb%29-no-seu-Linux?

clear
echo
echo "Instalando o \"mdbtools\"!"
echo "Continuar instalacao?"
echo "y -> yes"
echo "ctr+c -> cancelar"
read op

if [ $op == 'y' ] ; then
	if [[ `id -u` -ne 0 ]] ; then 
		echo "Por favor, roda como root!"; 
		exit 1; 
	fi


	apt-get install mdbtools
	apt-get install mdbtools-gmdb 
	echo "Para abrir digite no terminal como usuario nao root\"gmdb2 \""
fi

echo "done"
echo "-------"
