# Monitorar rede
clear
echo
echo "_--* Monitorando rede *--_"
echo
echo "iptraf -i wlan0 ou eth0"
echo "iftop -i wlan0 ou eth0"
echo "speedometer -r eth0 -t eth0"
echo "bmon"
echo

# Processos
echo "_--* Monitorando Processos *--_"
echo
echo "top"
echo "htop"
echo
echo "============="
