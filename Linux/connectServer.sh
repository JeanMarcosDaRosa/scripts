# Script para conectar aos servidores da Transend
# Teremos ao lado de cada servidor serviço que esta rodando

clear
echo "====================================================="
echo "Conectando aos servidores da Transend"
echo "1 -> Servidor 1 (5.189.144.4)	[Banco Cassandra - \"Replica do servidor 5\"]"
echo "2 -> Servidor 2 (79.143.185.3)"
echo "3 -> Servidor 3 (91.205.172.85)"
echo "4 -> Servidor 4 (91.205.174.79)"
echo "5 -> Servidor 5 (213.136.81.102)	[Banco Cassandra - seed]"
echo "6 -> Servidor 6 (213.136.82.35)"
echo "7 -> Servidor 7 (213.136.83.227)"
echo "8 -> Servidor 8 (213.136.83.226)"
echo "9 -> Servidor 9 (213.136.83.225)"
echo "10 -> Servidor 10 (213.136.83.224)"
echo ""
echo "__..--** Smartdash **--..__"
echo "11 - > Servidor Smartdash (104.131.194.196)"
echo "12 - > LogStalgia no Servidor Smartdash (104.131.194.196)"
echo ""
echo "Sair -> CTRL+C"



read opcao
case $opcao in
	1) echo "Conectando o servidor 1" && ssh root@5.189.144.4 ;;
	2) echo "Conectando o servidor 2" && ssh root@79.143.185.3 ;;
	3) echo "Conectando o servidor 3" && ssh root@91.205.172.85 ;;
	4) echo "Conectando o servidor 4" && ssh root@91.205.174.79 ;;
	5) echo "Conectando o servidor 5" && ssh root@213.136.81.102 ;;
	6) echo "Conectando o servidor 6" && ssh root@213.136.82.35 ;;
	7) echo "Conectando o servidor 7" && ssh root@213.136.83.227 ;;
	8) echo "Conectando o servidor 8" && ssh root@213.136.83.226 ;;
	9) echo "Conectando o servidor 9" && ssh root@213.136.83.225 ;;
	10) echo "Conectando o servidor 10" && ssh root@213.136.83.224 ;;
	11) echo "Conectando ao servidor Smartdash" && ssh root@104.131.194.196;;
	12) echo "Iniciando logStalgia no servidor Smartdash" && ssh root@104.131.194.196 "tail -f /var/log/apache2/access.log" | logstalgia -;;
	*) echo "Servidor invalido" ;;
esac
