# Instalacao do Ambiente Grafico MATE no Debian Wheezy
#
# instalacao completa do ambiente MATE com extras
# apt-get install mate-desktop-environment-extras

# instalacao completa do ambiente Mate
# apt-get install mate-desktop-environment

# instalacao basica do Mate
# apt-get install mate-desktop-environment-core


if [[ `id -u` -ne 0 ]] ; then echo "Por favor, execute como root!" ; exit 1 ; fi

echo "" >> /etc/apt/sources.list
echo "# add repository from wheezy-backports to install GMate"
echo "deb http://ftp.de.debian.org/debian wheezy-backports main" >> /etc/apt/sources.list
echo "deb-src http://ftp.de.debian.org/debian wheezy-backports main" >> /etc/apt/sources.list

apt-get update

echo "============================="
echo ""
echo "Instalacao Core do Mate >> 1"
echo "Instalacao completa >> 2"
echo "Instalacao completa extras >> 3"
read opcao

case $opcao in
	1) echo "Instalando a instalacao mais basica do Ambiente Mate" && apt-get install mate-desktop-environment-core;;
	2) echo "Instalando a instalacao completa do Ambiente Mate" && apt-get install mate-desktop-environment;;
	3) echo "Instalando a instalacao completa do Ambiente Mate com extras" && apt-get install xorg mate-desktop-environment-extras;;
	*) echo "Opcao invalida!";;
esac

echo ""
echo "---------"
echo "Done"

# Fabio Dornel
# 21/02/2015
