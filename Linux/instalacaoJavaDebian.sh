if [[ `id -u` -ne 0 ]] ; then echo "Por favor, execute como root!" ; exit 1 ; fi

echo "" >> /etc/apt/sources.list
echo "# Java" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" >> /etc/apt/sources.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" >> /etc/apt/sources.list

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
apt-get update
apt-get install oracle-java7-installer

echo "done"