#-----------------------------------------------------------------------------
# Script de Criacao das Filas
#
# Versao 1.9
# Data 15/01/2014
# Autor Gustavo Dalla Nora
#-----------------------------------------------------------------------------
import java.lang.System as jsystem
import java.lang.String as jstring
import sys

redirect('./logs/createQueues.Server.log', 'true')
sys.stdout = open('./logs/createQueues.log', 'w')
print "Usando propriedades de: " + sys.argv[1:][0]
loadProperties(sys.argv[1:][0])

print "Importando queuesconfig...(Versao 1.9) "
import queuesconfig

print('Running Scriptgen WLST script...')
REDELIVERY_LIMIT = 2
REDELIVERY_DELAY = 10000
EXPIRATION_POLICY = 'Redirect'

EXPIRATION_POLICY_ERR = 'Log'
EXPIRATION_LOG_INFO = '%header%,%properties%'

#-----------------------------------------------------------------------------
# connect/read domain
#-----------------------------------------------------------------------------
print "...connecting to server " + serverName
connect(webLogicAdminUser,webLogicAdminPass,'t3://'+webLogicHost+':'+webLogicT3Port)
edit()
startEdit()
if isCluster == "false":
	targetObj = getMBean('Servers/' + serverName)
	if None == targetObj:
		print 'Error: no mbean server for ', serverName
		exit()
else:
	targetObj = getMBean('Clusters/' + clusterName)
	if None == targetObj:
		print 'Error: no mbean server for ', clusterName
		exit()

if isCluster == "false" :
	QUEUE_TYPE = 'Queue'
else :
	QUEUE_TYPE = 'UniformDistributedQueue'

#!-----------------------------------------------------------------------------
#! Create Async Dispatcher queues
#!-----------------------------------------------------------------------------
#   queue =  QUEUE_PAIRS[0]
for queue in queuesconfig.QUEUE_LIST :
	QUEUE_NAME = queue[queuesconfig.QUEUE_NAME]+'Queue'
	ERROR_QUEUE_NAME = queue[queuesconfig.QUEUE_NAME]+'ErrorQueue'
	QUEUE_JMSSERVER = queue[queuesconfig.QUEUE_JMSSERVER]+'JMSServer'
	QUEUE_JMSMODULE = queue[queuesconfig.QUEUE_JMSMODULE]+'JMSModule'
	SUBDEPLOYMENT_NAME = QUEUE_JMSSERVER
	CONN_FACTORY_NAME = queue[queuesconfig.QUEUE_NAME]+'ConnectionFactory'
	JMS_JNDI_PREFIX = queuesconfig.JMS_JNDI_PREFIX
	print 'Queue Config for:', QUEUE_NAME, ERROR_QUEUE_NAME, QUEUE_JMSSERVER, QUEUE_JMSMODULE, JMS_JNDI_PREFIX, CONN_FACTORY_NAME
	cd ('/')
	jmssystemresource = getMBean('JMSSystemResources/'+QUEUE_JMSMODULE)
	if jmssystemresource is None:
		print '...creating', QUEUE_JMSMODULE
		jmssystemresource = create(QUEUE_JMSMODULE,'JMSSystemResource')
		jmssystemresource.addTarget(targetObj)
	else:
		print QUEUE_JMSMODULE, ' JMSSystemResource exists...'
	jmsresource = jmssystemresource.getJMSResource();
	myCF = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/JMSResource/"+QUEUE_JMSMODULE+"/ConnectionFactories/"+CONN_FACTORY_NAME)
	if myCF is None:
		print '...creating', CONN_FACTORY_NAME
		myCF = jmsresource.createConnectionFactory(CONN_FACTORY_NAME)
		print '...created CF'
		myCF.setJNDIName(JMS_JNDI_PREFIX+CONN_FACTORY_NAME)
		print '...set CF JNDI'
		if isCluster != "false":
			myCF.setDefaultTargetingEnabled(true)
	else:
		print CONN_FACTORY_NAME, ' ConnectionFactory exists...'
	if isCluster == "false":
		jmsserver = getMBean('JMSServers/'+QUEUE_JMSSERVER)
		if jmsserver is None:
			print '...creating', QUEUE_JMSSERVER
			jmsserver = create(QUEUE_JMSSERVER,'JMSServer')
			jmsserver.addTarget(targetObj)
		else:
			print QUEUE_JMSSERVER, ' JMSServer exists...'
		subdeployment = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/SubDeployments/"+SUBDEPLOYMENT_NAME)
		if subdeployment is None:
			print '...creating sub', SUBDEPLOYMENT_NAME
			subdeployment = jmssystemresource.createSubDeployment(SUBDEPLOYMENT_NAME)
			subdeployment.addTarget(jmsserver)
		else:
			print SUBDEPLOYMENT_NAME, ' SubDeployment exists...'
		myCF.setSubDeploymentName(SUBDEPLOYMENT_NAME)
		myEQ = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/JMSResource/"+QUEUE_JMSMODULE+"/Queues/"+ERROR_QUEUE_NAME)
		if myEQ is None:
			print '...creating', ERROR_QUEUE_NAME
			myEQ = jmsresource.createQueue(ERROR_QUEUE_NAME)
			myEQ.setSubDeploymentName(SUBDEPLOYMENT_NAME)
			myEQ.setJNDIName(JMS_JNDI_PREFIX+ERROR_QUEUE_NAME)
		else:
			print ERROR_QUEUE_NAME, ' Error Queue exists...'
		myQ = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/JMSResource/"+QUEUE_JMSMODULE+"/Queues/"+QUEUE_NAME)
		if myQ is None:
			print '...creating', QUEUE_NAME
			myQ = jmsresource.createQueue(QUEUE_NAME)
			myQ.setSubDeploymentName(SUBDEPLOYMENT_NAME)
			myQ.setJNDIName(JMS_JNDI_PREFIX+QUEUE_NAME)
		else:
			print QUEUE_NAME, ' Queue exists...'
	else:
		myEQ = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/JMSResource/"+QUEUE_JMSMODULE+"/UniformDistributedQueues/"+ERROR_QUEUE_NAME)
		if myEQ is None:
			print '...creating', ERROR_QUEUE_NAME
			myEQ = jmsresource.createUniformDistributedQueue(ERROR_QUEUE_NAME)
			myEQ.setDefaultTargetingEnabled(true)
			myEQ.setJNDIName(JMS_JNDI_PREFIX+ERROR_QUEUE_NAME)
		else:
			print ERROR_QUEUE_NAME, 'Error Queue exists...'
		myQ = getMBean("/JMSSystemResources/"+QUEUE_JMSMODULE+"/JMSResource/"+QUEUE_JMSMODULE+"/UniformDistributedQueues/"+QUEUE_NAME)
		if myQ is None:
			print '...creating', QUEUE_NAME
			myQ = jmsresource.createUniformDistributedQueue(QUEUE_NAME)
			myQ.setDefaultTargetingEnabled(true)
			myQ.setJNDIName(JMS_JNDI_PREFIX+QUEUE_NAME)
		else:
			print QUEUE_NAME, ' Queue exists...'
	print 'Seting Params...',CONN_FACTORY_NAME
	myCF.getTransactionParams().setXAConnectionFactoryEnabled(true)
	print 'Seting Params...',QUEUE_NAME
	myQFailureParams = myQ.getDeliveryFailureParams()
	myQFailureParams.setErrorDestination(myEQ)
	myQFailureParams.setRedeliveryLimit(REDELIVERY_LIMIT)
	myQFailureParams.setExpirationPolicy(EXPIRATION_POLICY)
	myQ.getDeliveryParamsOverrides().setRedeliveryDelay(REDELIVERY_DELAY)
	print 'Seting Params...',ERROR_QUEUE_NAME
	myEQFailureParams = myEQ.getDeliveryFailureParams()
	myEQFailureParams.setExpirationPolicy(EXPIRATION_POLICY_ERR)
	myEQFailureParams.setExpirationLoggingPolicy(EXPIRATION_LOG_INFO)

#!-----------------------------------------------------------------------------
#! Update and close domain
#!-----------------------------------------------------------------------------
print '...saving and activating changes'
save()
activate()
print 'Done'

stopRedirect()

#exit()
