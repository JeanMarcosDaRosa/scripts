#-----------------------------------------------------------------------------
# Configurações das filas para o Script de Criacao das Filas
#
# Versao 1.5
# Data 11/12/2013
# Autor Gustavo Dalla Nora
#-----------------------------------------------------------------------------

QUEUE_NAME = 0
QUEUE_JMSSERVER = 1
QUEUE_JMSMODULE = 2
JMS_JNDI_PREFIX = "pontoslivelo.jms."
QUEUE_LIST=[ ["MMEventRetry","SharedComponents","SharedComponents"], ["MMEvent","SharedComponents","SharedComponents"]]
