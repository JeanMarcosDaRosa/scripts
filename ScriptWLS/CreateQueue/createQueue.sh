#!/bin/bash

# Verifica se é root, se for, entao eh abortado a execucao
if [[ `id -u` == 0 ]] ; then echo "Por favor, nao execute como root!" ; exit 1 ; fi

# Local da instalacao od Oracle12c
echo "-------------------------"
DOMAIN_HOME=/home/fabio/Oracle/Middleware/Oracle_Home
echo "Utilizando como home do dominio: " $DOMAIN_HOME

# Setando o diretorio local para criar a pasta que contera os logs
DIRETORIO_LOCAL=$(pwd)
echo "Diretorio local: "$DIRETORIO_LOCAL
echo "---------------"

if [ ! -d  $DIRETORIO_LOCAL/logs ]; then
  mkdir $DIRETORIO_LOCAL/logs
  echo "Criando Diretorio de logs: ". $DIRETORIO_LOCAL/logs
else
  echo "Diretorio de logs ja existente!"
fi

$DOMAIN_HOME/wlserver/common/bin/wlst.sh createQueues.py env.localhost.properties
