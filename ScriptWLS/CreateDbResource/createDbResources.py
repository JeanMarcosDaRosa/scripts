import os
import time
from java.io import FileInputStream

propInputStream = FileInputStream("details.properties")
configProps = Properties()
configProps.load(propInputStream)

# Propriedades de configuracao
domainName = configProps.get("domainname")
adminURL= configProps.get("adminurl")
adminUserName= configProps.get("adminusername")
adminPassword= configProps.get("adminpassword")
dsName= configProps.get("datasourcename")
dsFileName= configProps.get("datasourcefilename")
dsDatabaseName=configProps.get("datasourcedatabasename")
datasourceTarget=configProps.get("datasourcetarget")
dsJNDIName=configProps.get("datasourcejndiname")
dsDriverName=configProps.get("datasourcedriverclass")
dsURL=configProps.get("datasourceurl")
dsUserName=configProps.get("datasourceusername")
dsPassword=configProps.get("datasourcepassword")
dsTestQuery=configProps.get("datasourcetestquery")
CFName=configProps.get("connfactname")
TargetServelrName=configProps.get("targetservername")
oracleHome = configProps.get("oraclehome")
appPath= oracleHome + configProps.get("apppath")
oracleHome + configProps.get("plan")
appName=configProps.get("appname")
moduleOverrideName=appName+ configProps.get("moduleoverridename")
moduleDescriptorName=configProps.get("moduledescriptorname")
connect(adminUserName, adminPassword, adminURL)
createDataSource=configProps.get("createdatasource")
createCF=configProps.get("createcf")
associateDataSourceWithCF=configProps.get("associatedatasourcewithcf")

print '\n\n#####################'
print 'Create Data Source [',createDataSource, ']'
print 'Create Connection Factory[', createCF, ']'
print 'Associate DS with CF [', associateDataSourceWithCF, ']'
print '#####################\n\n'

# Funcao simples para validar strings que devem ter valores boleanos.
# Se tudo estiver certo nas validacoes devolve uma string com valor boleano em
# caixa alta
def validaStringsBooleanas(var):
    if var.upper() == 'TRUE':
        return 'TRUE'
    if var.upper() == 'FALSE':
        return 'FALSE'

    raw_input ('Valores nao boleanos! Valor ['+var+']\nEnter para continuar...')
    exit()


createDataSource = validaStringsBooleanas(createDataSource)
createCF = validaStringsBooleanas(createCF)
associateDataSourceWithCF = validaStringsBooleanas(associateDataSourceWithCF)

# Criacao do DataSource
if createDataSource == 'TRUE':
    edit()
    startEdit()
    cd('/')
    cmo.createJDBCSystemResource(dsName)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName)
    cmo.setName(dsName)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDataSourceParams/' + dsName )
    set('JNDINames',jarray.array([String('jdbc/' + dsName )], String))
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDriverParams/' + dsName )
    cmo.setUrl(dsURL)
    cmo.setDriverName( dsDriverName )
    cmo.setPassword(dsPassword)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCConnectionPoolParams/' + dsName )
    cmo.setTestTableName(dsTestQuery)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDriverParams/' + dsName + '/Properties/' + dsName )
    cmo.createProperty('user')
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDriverParams/' + dsName + '/Properties/' + dsName + '/Properties/user')
    cmo.setValue(dsUserName)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDriverParams/' + dsName + '/Properties/' + dsName )
    cmo.createProperty('databaseName')
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDriverParams/' + dsName + '/Properties/' + dsName + '/Properties/databaseName')
    cmo.setValue(dsDatabaseName)
    cd('/JDBCSystemResources/' + dsName + '/JDBCResource/' + dsName + '/JDBCDataSourceParams/' + dsName )
    cmo.setGlobalTransactionsProtocol('TwoPhaseCommit')
    cd('/SystemResources/' + dsName )
    set('Targets',jarray.array([ObjectName('com.bea:Name=' + datasourceTarget + ',Type=Server')], ObjectName))
    save()
    # Fim da Criacao do DataSource
    activate()
    print '\n\nData source ' + dsName + ' configured\n\n'

# Essa funcao apenas faz a implantacoes
def makeDeploymentPlanVariable(wlstPlan, name, value, xpath, origin='planbased'):
    while wlstPlan.getVariableAssignment(name, moduleOverrideName, moduleDescriptorName):
        wlstPlan.destroyVariableAssignment(name, moduleOverrideName, moduleDescriptorName)

    while wlstPlan.getVariable(name):
        wlstPlan.destroyVariable(name)

    variableAssignment = wlstPlan.createVariableAssignment( name, moduleOverrideName, moduleDescriptorName )
    variableAssignment.setXpath( xpath )
    variableAssignment.setOrigin( origin )
    wlstPlan.createVariable( name, value )

def main():
        connect(adminUserName, adminPassword, adminURL)
        edit()
        try:
            startEdit()
            planPath = get('/AppDeployments/DbAdapter/PlanPath')
            print '\n<< Using plan ' + planPath + ' >>\n'
            myPlan=loadApplication(appPath, planPath)
            print '\n\n___ BEGIN change plan'


            #Cria CF
            if createCF == 'TRUE':
                makeDeploymentPlanVariable(myPlan, 'ConnectionInstance_'+ CFName , CFName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="'+ CFName + '"]/jndi-name')

                print '\n\t << Criado uma ConnectFactory [', CFName, '] >>'


            # Associa DT com CF
            if associateDataSourceWithCF == 'TRUE':
                makeDeploymentPlanVariable(myPlan, 'ConfigProperty_xADataSourceName_'+ dsName ,dsName, '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="'+ CFName + '"]/connection-properties/properties/property/[name="xADataSourceName"]/value')

                print '\n\t << Criado associacao entre CF [', CFName, '] com DT [', dsName, '] >>'

            print '___ DONE change plan\n\n'
            myPlan.save();
            save();
            activate(block='true');
            cd('/AppDeployments/DbAdapter/Targets');

            redeploy(appName, planPath,targets=cmo.getTargets());
            print 'EIS Connection factory ' + CFName + 'using' + dsName + ' configured';
        except Exception, e:
            print 'Error [', e, ']'
            stopEdit('y')


main()
