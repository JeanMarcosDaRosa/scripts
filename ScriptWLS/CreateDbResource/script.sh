#!/bin/bash

# Verifica se é root, se for, entao eh abortado a execucao
if [[ `id -u` == 0 ]] ; then echo "Por favor, nao execute como root!" ; exit 1 ; fi

# Diretorio de instalacao do ambiente OracleSOA12c
Oracle_Home=/home/fabio/Oracle2/

# Definicao de do diretorio atual para criar os logs
# Caminho da pasta nao pode conter espacos, ex: /home/meu Teste/    << isso ira provar erro
Diretorio_Local=/home/fabio/scripts/ScriptWLS

echo ""
echo "Usando home [". $Oracle_Home."]"
echo "Diretorio local [". $Diretorio_Local ."]"

# Criando o diretorio de log com nome logs
if [ ! -d  $Diretorio_Local/logs ]; then
  mkdir $Diretorio_Local/'logs'
  echo "Criando Diretorio de logs [". $Diretorio_Local/logs ."]"
else
  echo "Diretorio de logs ja existente!"
fi

# Iniciando a execucao do script com encaminhamento de saida para o arqauivo de
# log.
# OBS: no arquivo de log nunca sera apagado seu conteudo, sempre acrescentado
$Oracle_Home/wlserver/common/bin/wlst.sh createDbResources.py >> $Diretorio_Local/logs/log.txt

echo "" >> $Diretorio_Local/logs/log.txt
echo "Fim execucao" >> $Diretorio_Local/logs/log.txt
echo "" >> $Diretorio_Local/logs/log.txt

echo ""
echo "Feito"
