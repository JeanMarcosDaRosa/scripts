import math
from decimal import Decimal
from termcolor import colored
import matplotlib.pyplot as plt


# Classe com o onjetivo de ter todos os tipos de calculso para o corpo humano
# OSB: esses aqui é uns que eu tenho, se tiver mais seria legal implementar

class CalculosCorpoHumano():

	# Calculo da densidade do corpo do homem
	# Dados como coxa, peitoral e adomen devem estar em centimetros
	def densidadeHomen(idade, coxa, peitoral, abdomen):
		return Decimal(1.10938 - 0.0008267 * (coxa + abdomen + peitoral)
                    + 0.0000016 * (coxa + abdomen + peitoral) * (coxa + abdomen
                    + peitoral) - 0.0002575 * idade)

	# Calculo da densidade do corpo da mulher
	# Dados como coxa, triceps e supraIliaca devem estar em centimetros
	def densidadeMulher(idade, coxa, triceps, supraIliaca):
		return Decimal(1.099492 - 0.0009929 * (triceps + supraIliaca + coxa)
	                        + 0.0000023 * (triceps + supraIliaca + coxa)
	                        * (triceps + supraIliaca + coxa) - 0.0001392 * idade)

	# Calculo para percentula de gordura necessita de dados da densidade, isso deve
	# ser levar em conta que para diferentes sexos temos diferentes modos de obter
	# a densidade
	def percentualGordura(densidade):
		return Decimal((4.95 / densidade - 4.5)*100)

	# Retorna o IMC
	def imc(peso, altura):
		return Decimal(peso/math.pow(altura, 2))

	# Tabela de avaliaco do IMC. Ela contem dados que informa se o IMC esta normal...
	def tabAvaliacaoIMC(imc):
		if imc < 18.5:
			return 'Abaixo do peso'
		elif imc >= 18.5 and imc < 25:
			return 'IMC ideal!'
		elif imc >= 25 and imc < 30:
			return 'Levemente acima do peso'
		elif imc >= 30 and imc < 35:
			return 'Primeiro Grau de Obesidade'
		elif imc >= 35 and imc < 40:
			return 'Segundo Grau de Obesidade'
		elif imc >= 40:
			return 'Obesidade Morbida'

		# Circunferencia da cintura em cm e Cincunferência do quadril tambem em cm
	def PCCQ(circCintura, circQuadril):
		return Decimal(circCintura/circQuadril)

	def flexibilidadeMaxima(idade):
		return Decimal(220-idade)

	# Para mostrar as avaliacoes em graficos precido do numero de avaliacoes,
	# lista de imc de cada avaliacao,
	# lista de percentualGordura de cada avaliacao
	# lista de densidade de cada avaliacao
	def plotarDados(totalAvaliacoes, imc, percentualGordura, densidade):

		# Gera uma lista (eixo x)
		x = list(range(totalAvaliacoes))

		plt.plot(x, percentualGordura, marker='o', label=u'Percentual de Gordura', color='green')
		plt.legend(loc='upper left')
		plt.grid(True)
		plt.xlabel('Avaliacao')
		plt.ylabel('Percentual Gordura')
		plt.show()

		plt.plot(x, imc, marker='o', label=u'IMC', color='black')
		plt.legend(loc='upper left')
		plt.grid(True)
		plt.xlabel('Avaliacao')
		plt.ylabel('IMC')
		plt.show()

		plt.plot(x, densidade, marker='o', label=u'Densidade', color='red')
		plt.legend(loc='upper left')
		plt.grid(True)
		plt.xlabel('Avaliacao')
		plt.ylabel('Densidade')
		plt.show()

		# Listando em um só gráfico
		plt.plot(x, percentualGordura, marker='H', label=u'Percentual de Gordura', color='green')
		plt.plot(x, imc, marker='*', label=u'IMC', color='black')
		plt.plot(x, densidade, marker='D', label=u'Densidade', color='red')

		plt.axis((0, len(x)+1, 0, 60))
		plt.title('Graficos')
		plt.legend(loc='upper left')
		plt.grid(True)
		plt.xlabel('Avaliacao')
		plt.ylabel('Dados')
		plt.show()




if __name__ == '__main__':
	calc = CalculosCorpoHumano

	# Parametros para método do calculo da densidadeHomem
	# idade, circunferencia coxa (cm), circunferencia peitoral(cm), circunferencia adbomen(cm)
	print (colored('Teste de calculo de densidade do corpo do homem', 'red'))
	print (colored('Resultado <<>> ', 'green'),calc.densidadeHomen(25, 45, 95, 56))

	# Parametros para método do calculo da densidadeMulher
	# idade, circunferência da coxa (cm), circunferência do triceps(cm) e supraIliaca
	print (colored('Teste de calculo de densidade do corpo da mulher', 'red'))
	print (colored('Resultado <<>> ', 'green'), calc.densidadeMulher(21, 67, 23, 23))

	# Todos os calulos daqui para baixo são independes de sexo, ou seja,
	# vale tanto para sexo femenino e masculino
	print (colored('\nTestes para:', 'red'), colored('\n\tIMC;\n\tTabela de avaliacao de IMC;','yellow'),
		colored('\n\tPercenturalGordura;\n\tPCCQ;\n\tFlexibilidade Maxima.', 'yellow'))


	print (colored('IMC: ', 'blue'), calc.imc(73,1.7))
	print (colored('Tabela de avaliacao IMC: ', 'blue'), calc.tabAvaliacaoIMC(25.56))
	print (colored('PCCQ: ', 'blue'), calc.PCCQ(56, 65))
	print (colored('FMax; ', 'blue'), calc.flexibilidadeMaxima(25))
	print (colored('Percentual de Gordura', 'blue'), calc.percentualGordura(float(calc.densidadeHomen(25, 45, 95, 56))))

	print (colored('\n\n<<< Teste com dados de avaliacoes para gerar graficos >>>', 'red'))
	# idade, coxa, peitoral, abdomen, peso, altura
	mes = {'Avaliacao1':[25, 45, 95, 56, 73, 1.7], 'Avaliacao2':[25, 45, 90, 56, 70, 1.7], 'Avaliacao3':[25, 40, 88, 54, 71, 1.7],
	'Avaliacao4':[25, 45, 95, 56, 68, 1.7], 'Avaliacao5':[32, 45, 95, 56, 71, 1.7]}

	percentGordura = []
	imc = []
	densidade = []
	for m in mes:
		percentGordura.append(calc.percentualGordura(float(calc.densidadeHomen(mes[m][0], mes[m][1], mes[m][2], mes[m][3]))))
		imc.append(calc.imc(mes[m][4], mes[m][5]))
		densidade.append(calc.densidadeHomen(mes[m][0], mes[m][1], mes[m][2], mes[m][3]))

	print (colored('Percent Gordura ', 'red'), percentGordura)
	print (colored('IMC ', 'red'), imc)
	print (colored('Densidade', 'red'), densidade)
	print (colored('Total avaliacoes', 'red'), len(mes))


	# totalAvaliacoes, imc, percentualGordura, densidade
	calc.plotarDados(len(mes), imc, percentGordura, densidade)

# Exemplo de estrutura de dados de uma pessoa:
# Acho que estaria se o treino tivesse uma array de avaliações
#{
#  "nome":"Fabio",
#  "dataNascimento":"10/10/1989",
#  "avaliacao":{
#    "avalaicao1":[25, 45, 95, 56, 73, 1.7],
#    "avalaicao2":[25, 45, 95, 56, 68, 1.7]
#  },
#  "treinos":{
#    "treino1":{"seilaoque":"peso", "seilaoq":"pesoa"},
#    "treino1":{"seilaoque":"peso", "seilaoq":"pesoa"}
#  }
#}
