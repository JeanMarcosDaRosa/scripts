def calculoPesoAltura(altjoelho=0, cirbraco=0, idade=0, sexo=1):
	altimc = pesoimc = 0
	if sexo == 1:
		if idade < 19:
			pesoimc, altimc = ((altjoelho * 0.68) + (cirbraco * 2.64) - 50.08), ((altjoelho * 1.88) + 71.85)
		elif idade < 60:
			pesoimc, altimc = ((altjoelho * 1.19) + (cirbraco * 3.21) - 86.82), ((altjoelho * 1.88) + 71.85)
		else:
			pesoimc, altimc = ((altjoelho * 1.10) + (cirbraco * 3.07) - 75.81), (64.19 - (0.04 * idade) + (2.02 * altjoelho))
	else:
		if idade < 19:
			pesoimc, altimc = ((altjoelho * 0.77) + (cirbraco * 2.47) - 50.16), ((1.87 * altjoelho) - (0.06 * idade) + 70.25)
		elif idade < 60:
			pesoimc, altimc = ((altjoelho * 1.01) + (cirbraco * 2.81) - 66.04), ((1.87 * altjoelho) - (0.06 * idade) + 70.25)
		else:
			pesoimc, altimc = ((altjoelho * 1.09) + (cirbraco * 2.68) - 65.51), (84.88 - (0.24 * idade) + (1.83 * altjoelho))
	return altimc, pesoimc, (pesoimc / (altimc * altimc)) * 10000
print("Altura: %s\nPeso: %s\nIMC: %s\n" % calculoPesoAltura(50, 36, 21, 1))