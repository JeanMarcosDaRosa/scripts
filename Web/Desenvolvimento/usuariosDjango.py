__author__ = 'jean.marcos'
from django.contrib.auth import get_user_model

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, Page
from django.views.generic import TemplateView

class FlynsarmyPaginator(Paginator):
    def __init__(self, object_list, per_page, orphans=0, allow_empty_first_page=True, adjacent_pages=6):
        self.adjacent_pages = adjacent_pages
        super(FlynsarmyPaginator, self).__init__(object_list, per_page, orphans, allow_empty_first_page)

    def page(self, number):
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        return FlynsarmyPage(self.object_list[bottom:top], number, self, self.adjacent_pages)

class FlynsarmyPage(Page):
    def __init__(self, object_list, number, paginator, adjacent_pages=6):
        self.adjacent_pages = adjacent_pages
        super(FlynsarmyPage, self).__init__(object_list, number, paginator)

    def _get_page_range_data(self):
        """
        Returns a floating digg-style or 1-based  range of pages for
        iterating through within a template for loop.
        """
        if not self.adjacent_pages:
            return self.paginator.page_range

        startPage = max(1, self.number - self.adjacent_pages)
        #Be a bit smarter about start page
        if startPage <= 3: startPage = 1
        endPage = self.number + self.adjacent_pages + 1
        #Be a bit smarter about end page
        if endPage >= self.paginator.num_pages - 1: endPage = self.paginator.num_pages + 1

        page_range = [n for n in range(startPage, endPage) \
                if n > 0 and n <= self.paginator.count]

        return {
            'page_range': page_range,
            'show_first': 1 not in page_range,
            'show_last': self.paginator.num_pages not in page_range,
        }
    page_range_data = property(_get_page_range_data)


class UsuariosAdmin(TemplateView):
    template_name = 'intranet/administracao/usuarios/listar.html'

    def get_context_data(self, **kwargs):
        UserModel = get_user_model()
        context = super(UsuariosAdmin, self).get_context_data(**kwargs)

        lista_usuarios = UserModel.objects.all()
        paginator = FlynsarmyPaginator(lista_usuarios, 1) # quantidade de objetos por pagina

        usuarios=[]

        page = 1
        if 'page' in self.kwargs:
            page = self.kwargs['page']

        try:
            usuarios = paginator.page(page)
        except PageNotAnInteger:
            usuarios = paginator.page(1)
        except EmptyPage:
            usuarios = paginator.page(paginator.num_pages)


        context['object_list'] = usuarios
        context['object_range'] = range(1, paginator.num_pages+1)
        return context