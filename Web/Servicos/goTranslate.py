#!/usr/bin/python

import goslate
import sys
import os
import base64

# Retorna apenas o texto na linguagem escolhida
def tradutor(text, linguage):
	try:
		gs = goslate.Goslate()
		return gs.translate(text, linguage)

	except Exception, e:
		print 'Erro, por favor verifique sua conexao com a internet!\n Erro [', e,']'
		exit(1)

# Comandos reconhecidos pelo script, comando nao contidos nesta lista
# resultara na saida incorreta do script
def comandos(arg):
	listComands = ['-t', '-l', '--base64', '--debug', '--file', '-h']

	for l in listComands:
		if l == arg:
			return True

	return False

def ajuda():
	os.system('clear')
	print '__---**** Menu de ajuda ****---__\n'
	print ' -t\t[Informar texto entre aspas simples]'
	print ' -l\t[Informar a linguagem, ex: -l en ou -l pt ou -l pt-br]'
	print ' --debug\t\t[Informa algumas informacoes a mais que apenas o resultado]'
	print ' --file\t\t[Permite a traducao a partir de um arquivo]'
	print ' --base64\t\t[Entrada e saida dos parametros em base64]\n\n'
	
	exit(2)

# Inicio do programa 
def main():
	# Se nao houver nenhum parametro apenas exibe esta msg e sai fora do programa
	if len(sys.argv) == 1:
		print 'Para obter ajuda digite \'-h\''
		exit(2)

	if comandos(sys.argv[1]) ==  True:
		b64 = 'N' # Padrao para nao base64
		debug = 'F' # Padrao para nao exibir debug

		# caso o primeiro parametro seja o -h, entao eh exibido o menu ajuda
		# e posterior exibicao do menu sai do script
		if sys.argv[1] == '-h':
			ajuda()

		cont = 1
		if len(sys.argv) > 1:
			for arg in sys.argv:
				if comandos(arg) == True:
					if arg == '-t':
						if len(sys.argv)-1 >= cont and comandos(sys.argv[cont]) != True:
							text = sys.argv[cont]
						else:
							print '\n\tErro ao receber dados de um parametro.\nDados de um parametro nao pode ser outro parametro!'
							exit(1)
					elif arg == '-l':
						if len(sys.argv) >= cont and comandos(sys.argv[cont]) != True:
							linguage = sys.argv[cont]
						else:
							print '\n\tErro ao receber dados de um parametro.\nDados de um parametro nao pode ser outro parametro!'
							exit(1)
					elif arg == '--base64':
							b64 = 'S'
					elif arg == '--debug':
							debug = 'S'
					elif arg == '--file':
						if len(sys.argv) >= cont and comandos(sys.argv[cont]) != True:
							try:
								arq = open (sys.argv[cont], 'r')
								text = arq.read()
							except Exception, e:
								print 'Erro ao abrir arquivo [', e, ']'
						else:
							print '\n\tErro ao receber dados de um parametro.\nDados de um parametro nao pode ser outro parametro!'
							exit(1)
				cont += 1

			try:
				if text != '' and linguage != '' and b64 != '':
					if b64 == 'S':
						if debug == 'S':
							print 'Para idioma >> ', base64.b64decode(linguage)
							print 'Texto [\n', base64.b64decode(text), '\n]'
							print '\n\t<<< Inicio Resultado >>>\n\n', tradutor(base64.b64decode(text), base64.b64decode(linguage)), '\n\nFim Resultado\n-----------\n'
						print base64.b64encode(tradutor(base64.b64decode(text), base64.b64decode(linguage)).encode('utf-8'))
					else:
						if debug == 'S':
							print 'Para idioma >> ', linguage
							print 'Texto [\n', text, '\n]'
							print '\n\t<<< Inicio Resultado >>>\n\n', tradutor(text, linguage), '\n\nFim Resultado\n-----------\n'
						print tradutor(text, linguage.encode('utf-8'))
			except Exception, e:
				print '\n\tErro [', e, ']\nPor favor, verifique os parametros!'
				
	else:
		if len(sys.argv) == 3:
			print tradutor(sys.argv[1], sys.argv[2].encode('utf-8'))
		else:
			print 'Informe o texto e a linguagem.\nex:\n\ttranslate.py \'Plante terra\' en'
			
# Dando inicio ao programa		
main()
