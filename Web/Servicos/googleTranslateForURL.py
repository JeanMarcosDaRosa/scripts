#!/usr/bin/env python
import sys
import pycurl 
import subprocess,StringIO

from HTMLParser import HTMLParser 

idiomaI = 'en'
idiomaO = 'pt'
entrada = sys.argv[1]
palavra = entrada.replace(' ', '+')

url     = 'http://translate.google.com/translate_t?q=' + palavra + '&sl=' + idiomaI + '&tl=' + idiomaO

class Parser(HTMLParser):
  cont = 0
  def handle_starttag(self, tag, attrs):
    if (tag == 'span' and len(attrs) > 0):
      for atom in attrs:
        if (atom[0] == 'title' and atom[1] == entrada):
          self.cont = 1
    else:
      self.cont = 0
  def handle_data(self, text):
    if (self.cont == 1):
      print text
  def handle_endtag(self, tag):
    self.con = 0

strio   = StringIO.StringIO() 
curlobj = pycurl.Curl()

curlobj.setopt(pycurl.HTTPHEADER, [
      "User-Agent: Mozilla/5.001 (windows; U; NT4.0; en-us) Gecko/25250101",
      "Accept-Language: en-us,en;q=0.5",
    ])

curlobj.setopt(pycurl.URL, url)
curlobj.setopt(pycurl.WRITEFUNCTION, strio.write)
curlobj.perform() 
curlobj.close()

p   = Parser()
aux = p.feed( strio.getvalue() )

