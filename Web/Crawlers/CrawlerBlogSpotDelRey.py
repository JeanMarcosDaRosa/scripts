import urllib2
import sys
from BeautifulSoup import BeautifulSoup, SoupStrainer
import string
import threading

link = 'http://manualproprietario.blogspot.com.br/2010/07/ford-del-rey.html'

contPag = 0
idpost = ''

def getSoupPage(link):
	try:
		c = urllib2.urlopen(link)
		soup = c.read()
		return BeautifulSoup(soup)
	except:
		headers = {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5'}
		request = urllib2.Request(link, '', headers)
		try:
			data_Res = urllib2.urlopen(request)
		except:
			print 'Fail page: ', link
			return False		
		return data_Res

def getImages(div):
	global contPag
	global idpost
	for a in div:
		if getattr(a, 'name', None) == 'a':
			if(a.has_key('href')):
				print a['href']
				img = urllib2.urlopen(a['href'])
				output = open(idpost+'_'+str(contPag)+'.jpg','wb')
				output.write(img.read())
				output.close()
				contPag+=1
		elif getattr(a, 'name', None) == 'div':
			getImages(a)
# inicio

pag = getSoupPage(link)
pd = pag.findAll('meta', attrs={'itemprop':'postId'})
idpost = pd[0].attrs[0][1]
pd = pag.findAll('div', id='post-body-'+idpost)

for post in pd:
	getImages(post)

