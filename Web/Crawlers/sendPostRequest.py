import httplib
import urllib2

def printText(txt):
    lines = txt.split('\n')
    for line in lines:
        print line.strip()

httpServ = httplib.HTTPConnection("127.0.0.1", 8081)
httpServ.connect()

''' -----Dados insert comment----- '''
iduser='fake@domain.com'
nameuser='Jean Marcos da Rosa'
message="test of comment 3"
idpost="545cec2ee555c069f4271caa"
''' -----Dados insert post   ----- '''
listUsers='|fake@domain.com'
#for i in range(1,100001):
#    listUsers+='|'+str(i)+'@testedeinsercaonabasededadosmongocomservidornodejs.com.br'
idchannel='545ce5a5e555c0637cd81da9'
message='Teste de mensagem de canal revogado'
picture='http://i1.wp.com/www.nudelas.com/posts/karen-k-/karen-k-2.jpg'
type_='photo'
source='http://i1.wp.com/www.nudelas.com/posts/karen-k-/karen-k-2.jpg'
idextern=''
id_post_created='5460ba6dff0f81fc398ebd9c'

''' -----Envia para o servidor nodejs----- '''
msg=urllib2.quote(message)
#httpServ.request('POST', '/newcomment', 'idpost={0}&iduser={1}&nameuser={2}&message={3}'.format(idpost, iduser , nameuser, msg))
#httpServ.request('POST', '/createpost', 'idchannel={0}&message={1}&picture={2}&type={3}&source={4}&idextern={5}'.format(idchannel , message , picture , type_ , source , idextern))
httpServ.request('POST', '/topostonetomany', 'idpost={0}&userlist={1}'.format(id_post_created, listUsers))

response = httpServ.getresponse()
if response.status == httplib.OK:
    print "Output from node-js request"
    printText (response.read())

httpServ.close()