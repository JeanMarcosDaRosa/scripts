# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'coligadas'

SPIDER_MODULES = ['scraper_coligadas.spiders']

ITEM_PIPELINES = ['scraper_coligadas.pipelines.ColigadasPipeline']

DATABASE = {
    'drivername': 'postgres',
    'host': 'localhost',
    'port': '5432',
    'username': 'postgres',
    'password': 'nobug95',
    'database': 'python_tests'
}
