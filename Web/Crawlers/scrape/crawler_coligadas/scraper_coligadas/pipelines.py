#! -*- coding: utf-8 -*-

from sqlalchemy.orm import sessionmaker
from models import Imovel, db_connect, create_imoveis_table


class ColigadasPipeline(object):

    def __init__(self):
        engine = db_connect()
        create_imoveis_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()
        deal = Imovel(**item)
        try:
            session.add(deal)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
        return item
