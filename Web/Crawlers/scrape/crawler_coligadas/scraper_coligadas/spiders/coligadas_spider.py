#! -*- coding: utf-8 -*-

from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader
from scrapy.contrib.loader.processor import Join, MapCompose
from scraper_coligadas.items import ColigadasImovel


class ColigadasSpider(BaseSpider):
    name = "coligadas"
    allowed_domains = ["coligadas.com.br"]
    start_urls = ["https://www.coligadas.com.br"]

    imoveis_list_xpath = '//li[@dealid]'
    item_fields = {
        'title': './/title/text()',
    }

    def parse(self, response):
        selector = HtmlXPathSelector(response)

        # iterate over deals
        for imovel in selector.xpath(self.imoveis_list_xpath):
            loader = XPathItemLoader(ColigadasImovel(), selector=imovel)

            # define processors
            loader.default_input_processor = MapCompose(unicode.strip)
            loader.default_output_processor = Join()

            # iterate over fields and add xpaths to the loader
            for field, xpath in self.item_fields.iteritems():
                loader.add_xpath(field, xpath)
            yield loader.load_item()
