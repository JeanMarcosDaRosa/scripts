__author__ = 'jean.marcos'

import urllib.request as urllib2
import urllib.parse
from bs4 import BeautifulSoup
from os.path import basename, dirname
import os

link = 'http://manuaisnddcargo.nddigital.com.br/Auxilio%20Integra%C3%A7%C3%A3o'
linkRaiz = 'http://manuaisnddcargo.nddigital.com.br/'
dirDestino = 'C:/Users/jean.marcos/Desktop/CIOT/nddCargo'
histLinks = []

def getSoupPage(link):
	try:
		c = urllib2.urlopen(link)
		soup = c.read()
		return BeautifulSoup(soup)
	except:
		headers = {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5'}
		request = urllib2.Request(link, '', headers)
		try:
			data_Res = urllib2.urlopen(request)
		except:
			print('Fail page: ', link)
			return False
		return data_Res

def download(link, destino):
    print('Baixando:',link)
    arq = urllib2.urlopen(link)
    if not os.path.exists(destino):
        os.mkdir(destino)
    if (os.path.isdir(destino)):
        output = open(destino+'/'+urllib.parse.unquote(basename(link)),'wb')
        output.write(arq.read())
        output.close()
    else:
        print("Diretorio [",destino,"] invalido")

def getLinks(link):
	pag = getSoupPage(link)
	if(pag!=None and pag!=False):
		pd = pag.findAll('a')
		links = [ (link+'/'+a['href'], a['href'])[link in a['href']] for a in pd if hasattr(a, 'href') ]
		return links
	else:
		return []

def removeRaiz(linkRaiz, link):
    link = urllib.parse.unquote(link)
    linkRaiz = urllib.parse.unquote(linkRaiz)
    if link.startswith(linkRaiz):
        return link[len(linkRaiz):]
    else:
        return link

links = getLinks(link)

while(len(links)>0):
	novos = []
	for a in links:
		if not ('C=' in a and 'O=' in a or a == link+'//'):
			if a.endswith("/"):
				print('Diretorio ', a)
				if a not in histLinks:
					histLinks.append(a)
					linksNovos = getLinks(a)
					for l in linksNovos:
						if l not in histLinks:
							histLinks.append(l)
							novos.append(l)
			else:
				download(a, dirDestino+'/'+urllib.parse.unquote(dirname(removeRaiz(linkRaiz, a))))
	links = []
	links = [n for n in novos]