from selenium import webdriver
driver = webdriver.PhantomJS('C:\\phantomjs-2.0.0-windows\\bin\\phantomjs.exe')#
driver.get('http://pythonclub.com.br/')
posts = driver.find_elements_by_class_name('post')
for post in posts:
    post_title = post.find_element_by_class_name('post-title')
    post_link = post_title.get_attribute('href')
    print("Titulo: {titulo}, \nLink: {link}".format(titulo=post_title.text,link=post_link))
driver.quit()