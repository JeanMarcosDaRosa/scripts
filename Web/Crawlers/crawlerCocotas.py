#!/usr/bin/python
#coding: latin-1

import urllib
import urllib2
import sys
import os
import thread
import time
from BeautifulSoup import BeautifulSoup, SoupStrainer
import string
import threading
import cookielib

quebrados=[]
link = 'http://www.cocotasonfire.com/'
path = '/.git/galeria/cocotasonfire/'
qtdPosts=0
qtdImgs=0
contThreads = 0
contTotImagens=0

def getPageWithCookies(urlPagina):
  try:
    jar = cookielib.FileCookieJar("cookies")
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))
    opener.addheaders = {('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11')}
    pag = opener.open(urlPagina)
    return pag
  except:# Exception, e:           
    #print e,':', urlPagina
    return None;  

def getPage(urlPagina): 
  try:
    pag = urllib2.urlopen(urlPagina)
    return pag
  except Exception, e:
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    request = urllib2.Request(urlPagina, '', headers)
    try:
      pag = urllib2.urlopen(request)
      return pag
    except:
      return getPageWithCookies(urlPagina)
    
def processPageArt(linkPageArt, dm):
  global link, path, qtdPosts, qtdImgs, contThreads, quebrados
  soup = getPage(linkPageArt)
  if soup != None:
    soup = BeautifulSoup(soup.read())
    contImgs=0
    nomePasta = 'outras'
    nomeArq=0
    #Pega a data da postagem para colocar como nome da pasta
    tagsDataPubl = soup.findAll('meta', attrs={'property':'article:published_time'})
    for d in tagsDataPubl:
      if getattr(d, 'name', None) == 'meta':
	if(d.has_key('content')):
	  dataPubl = d['content']
	  if len(dataPubl)>10:
	    nomePasta=path+dataPubl[:10]+'/'
    
    metas = soup.findAll('meta', attrs={'property': 'og:url'})
    for n in metas:
      if getattr(n, 'name', None) == 'meta':
	if(n.has_key('content')):
	  nomeArq=n['content'][len(link):-1]
    
    tagsMeta = soup.findAll('meta', attrs={'property':'og:image'})
    idx=0
    if len(tagsMeta) > 2 :
      for m in tagsMeta:
	if getattr(m, 'name', None) == 'meta':
	  if(m.has_key('content') and m['content'].startswith(link)):
	    if idx == 0:
	      idx+=1
	      continue
	    nImg = m['content'].split('/')
	    defPath = str(nomePasta)+str(nomeArq)+'-'+str(contImgs)+(os.path.splitext(nImg[-1])[1])
	    if not os.path.isfile(defPath):
	      try:
		crop = 0
		out=''
		x=False
		for s in reversed(m['content']):
		  if s=='.' and crop == 0:
		    out=s+out
		    crop=1
		  elif s == '-' and crop == 1:
		    crop=2
		  elif crop != 1:
		    out=s+out
		  else:
		    if s.lower()=='x':
		      x=True
		    elif s.lower()=='/':
		      x=False
		      break
		if x == False:
		  out=m['content']
		img = getPageWithCookies(out.decode("latin-1"))
		if img == None:
		  img = getPageWithCookies(m['content'].decode("latin-1"))
		if img != None:
		  if not os.path.exists(nomePasta):
		    try:
		      os.mkdir(nomePasta)
		    except:
		      pass		
		  output = open(defPath,'wb')
		  output.write(img.read())
		  output.close()
		  contImgs+=1
		else:
		  print 'Erro 404:', m['content']
	      except:
		quebrados.append(out)
		#print 'Erro ao buscar imagem:', out
    qtdPosts+=1
    qtdImgs+=contImgs
    contThreads -= 1

def getLinksArt(pagina):
  global contThreads
  soup = getPage(pagina)
  if soup != None:
    soup = BeautifulSoup(soup.read())  
    divs = soup.findAll('div', attrs={'class':'item-thumb'})
    for div in divs:
      for a in div:
	if getattr(a, 'name', None) == 'a':
	  if a.has_key('href'):
	    thread.start_new_thread(processPageArt,(a['href'],0))
	    #processPageArt(a['href'],0)
	    contThreads+=1
  while contThreads>0:
    time.sleep(1)

total=0	
pag = getPage(link)
if pag != None:
  pag = BeautifulSoup(pag.read())
  tagsDiv = pag.findAll('div', id='pagination')
  for divs in tagsDiv:
    for ul in divs:
      if getattr(ul, 'name', None) == 'ul':
	for li in ul:
	  if li.getText().lower()[0:4] == 'last':
	    for a in li:
	      if getattr(a, 'name', None) == 'a':
		if a.has_key('href'):
		  if a['href'][-1]=='/':
		    a['href'] = a['href'][:-1]
		  y=a['href'].split("/")[-1]
		  x=int(y)
		  total=x
		  break

pgIni=1
pgFim=int(total)
if len(sys.argv)==2:
  pgIni = int(sys.argv[1])
elif len(sys.argv)==3:
  pgIni = int(sys.argv[1])
  pgFim = int(sys.argv[2])
elif len(sys.argv)==4:
  pgIni = int(sys.argv[1])
  pgFim = int(sys.argv[2])
  path = sys.argv[3]
  if path[-1:]!='/':
    path=path+'/'

print 'Salvando em:',path

if not os.path.exists(path):
  os.mkdir(path)
  
print 'Captura da pagina ['+str(pgIni)+'] ate ['+str(pgFim)+'] - do total de '+str(total)+' paginas'
for i in range(pgIni, pgFim+1):
  print 'Pagina numero [', i, ']'
  getLinksArt('http://www.cocotasonfire.com/page/'+str(i)+'/')
  print 'Total de Posts:', qtdPosts, ', Novas imagens:', qtdImgs
  print '-----------------------------------------------'
  contTotImagens+=qtdImgs
  qtdImgs=0
  qtdPosts=0
print 'Capturou no total ['+str(contTotImagens)+'] imagens'
print 'Total de links de imagens que falharam[',len(quebrados),']'
if len(quebrados) > 0:
  f=open('quebrados_cocotas.txt', 'a')
  for q in quebrados:
    f.write(q.encode('latin-1')+'\n')
  f.close()