# Crawler Animal


import urllib2, cookielib
import sys
from BeautifulSoup import BeautifulSoup, SoupStrainer
import string
import threading
import os

#link = 'http://thefappening.sexy/albums/'
raiz = "http://thefappening.sexy/albums/"
#link = 'http://thefappening.sexy/albums/index.php?/category/148'

def getTextTag(tag):
	tx=''
	if type(tag).__name__ == 'Tag':
	 tx=tag.getText(' ').encode('latin-1', 'ignore')
	if type(tag).__name__ == 'NavigableString':
	 tx=tag.encode('latin-1', 'ignore')
	return tx

def pegarPagina(link):
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
               'Accept-Encoding': 'none',
               'Accept-Language': 'en-US,en;q=0.8',
               'Connection': 'keep-alive'}
	
	req = urllib2.Request(link, headers=hdr)
	try:
	    page = urllib2.urlopen(req)
	except urllib2.HTTPError, e:
	    print e.fp.read()
	content = page.read()
	return content

def request():
	print 'Yes, I\'m over 18';
	
	
def downloadImg(link, path, recall):
	global contPag
	try:
		if path[:-1] != '/':
			path+='/'
		nome = str(link).split('/')
		if not os.path.isfile(path+(nome[len(nome)-1])):
			imdD = pegarPagina(link)		
			output = open(path+(nome[len(nome)-1]), 'wb')
			output.write(imdD)
			output.close()
	except Exception, e:
		if not recall:
			link=link.replace('-me', '')
			link=link.replace('_data/i/', '')
			downloadImg(link, path, True)
		else:
			print 'Error: ', e
	


def pegaAlbum(link):
	global raiz
	print 'Pega album'
	pag = BeautifulSoup(pegarPagina(link))
	listLinks = []
	if pag != False:
		links = pag.findAll('a')
		for l in links:
			if l.has_key('href'):
				if l['href'].startswith('index.php?/category/'):
					listLinks.append({getTextTag(l),raiz+str(l['href'])})
	else:
		print 'Erro ao buscar recurso'
	return listLinks


listLinks = pegaAlbum(raiz)
for t,l in listLinks:
	if t.startswith('http://'):
		x=t
		t=l
		l=x
	if not os.path.exists(t):
		os.mkdir(t)	
	p = BeautifulSoup(pegarPagina(l))
	images = p.findAll("img",attrs={'class':'thumbnail'} )
	for img in images:
		if img.has_key('src'):
			if img['src'].startswith('_data'):
				link = raiz+img['src'].replace('-th','-me')
				downloadImg(link, t, False)