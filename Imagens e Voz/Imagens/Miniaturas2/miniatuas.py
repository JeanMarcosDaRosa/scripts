from PIL import Image
import os, fnmatch
import configparser

# Leitura do arquivo de properties
details = configparser.RawConfigParser()
details.read('entrada.properties')

caminhos = []
for p in details.items('Path'):
    caminhos.append(p)

imagens = []
for i in details.items('Imagens'):
    imagens.append(i)

def find(pattern, path):
    print ('Nome[', pattern, '] ')
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return os.path.join(root, name)

def obtemOriginDestino(caminhos):
    try:
        for c in caminhos:
            if c[0] == 'origem':
                origem = c[1]
            else:
                destino = c[1]

        return [origem, destino]
    except Exception as e:
        print (e)

if __name__ == '__main__':
    x = obtemOriginDestino(caminhos)
    origem = x[0]
    destino = x[1]

    for img in imagens:
        try:
            nome = img[1].split('|')[0]
            tamanho = int(img[1].split('|')[1].split(',')[0]),int(img[1].split('|')[1].split(',')[1])

            img = Image.open(find(nome, origem))
            img.thumbnail(tamanho, Image.ANTIALIAS)
            img.save(destino+"miniatura_"+nome)
        except Exception as e:
            print (e)
