import cv2.cv as cv
import cv2 as cvt
import tesseract

#kernel = np.ones((5,5),np.uint8)
gray = cv.LoadImage('C:\images\captcha.jpg', cv.CV_LOAD_IMAGE_GRAYSCALE)
#img = cvt.imread('C:\\images\\captcha.jpg',0)
erosion = cvt.morphologyEx(gray, cvt.MORPH_OPEN, array([[0, 0, 1, 0, 0],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [1, 1, 1, 1, 1],
       [0, 0, 1, 0, 0]], dtype=uint8))
#cvt.imshow('Image', img)

#while True:
#	time.sleep( 5 )

cv.Threshold(erosion, erosion, 231, 255, cv.CV_THRESH_BINARY)
api = tesseract.TessBaseAPI()
api.Init("C:\\tesserdata\\tesseract-ocr","eng",tesseract.OEM_DEFAULT)
api.SetVariable("tessedit_char_whitelist", "0123456789abcdefghijklmnopqrstuvwxyz")
api.SetPageSegMode(tesseract.PSM_SINGLE_WORD)
tesseract.SetCvImage(erosion,api)
print api.GetUTF8Text()
