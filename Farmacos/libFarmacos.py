# Dependencias
# apt-get install python3-termcolor
# apt-get install python3-matplotlib


from decimal import Decimal
import math
from termcolor import colored
from pylab import *

class ConcentracaoPlasmatica():

	# Medicamnento via oral
	# Calculo concntracao plasmatica dentro da janela terapeutica
	def calculoTP(ka, ke, J):
		return Decimal((1/(ka-ke))*math.log((ka*(1-math.pow(math.e, -ke*J))/(ke*(1-math.pow(math.e, -ka*J))))))

	# Constante de eliminacao
	def calculoKE(cl, vd):
		return Decimal(cl/vd)

	# 
	def rinf(ke, J):
		return Decimal(1/(1-math.pow(math.e, -ke * J)))

	# Concentracao maxima do medicamento
	def cpssMAX(F, D, rinf, vd, ke, tp):
		return Decimal(((F*D)/vd)*(rinf * math.pow(math.e, -ke*tp)))

	# Concentracao minima do medicamento
	def cpssMIN(F, D, ka, ke, vd, rinf, J):
		return Decimal(((F*D*ka)/(vd*(ka-ke)))*(rinf*math.pow(math.e, -ke*J)))

# Modelo aberto monocompartimental
# Administracao VIA OROAL:  Analise de dados de concentracao Sanguinea
class ACSanguinea():

	# cf = concentracaoFinal
	# ci = concentracaoInicial
	# tf = tempoFinal
	# ti = tempoInicial
	def calculoKE_KA(cf, ci, tf, ti):
		return Decimal((math.log(cf)-math.log(ci))/(tf-ti))

	def tempoMeiaVidaAbsorcao(ka):
		return Decimal(math.log(2)/abs(ka))

	def analiseSituacaoNormal(ka, ke):
		if abs(ka) > abs(ke):
			return 'Situacao Normal'
		else:
			return 'Situacao Anormal'

	# Area sobre a concentracao tempo
	def asct(tempo, concentracao):
		i = 0
		final = 0
		for i in range(1, len(tempo)-1):
			final += (((concentracao[i]+concentracao[i+1])/2)*(tempo[i+1]-tempo[i])) 
		
		return Decimal(final)

	# Area sobre a concentracao extrapolada
	def ascExt(concentracaoFinal, ke):
		return Decimal(concentracaoFinal/abs(ke))

	def ascInfinito(asct, ascExt):
		return asct + ascExt

	def montarGrafico(tempo, tempoExtr_Resi, concentracao, concentracaoExtra, concentracaoResidual):
		plot(tempo, concentracao)
		plot(tempoExtr_Resi, concentracaoExtra)
		plot(tempoExtr_Resi, concentracaoResidual)
		show()

	def tMax(ka, ke):
		return (math.log(ka/ke))/(ka-ke)

	def calculoVD(cl, ke):
		return Decimal(cl / ke)

	def calculoCL(F, Dmg, ascInf):
		return Decimal((F * Dmg) / ascInf)

	def calculoCMAX(F, D, ke, tMax, vd):
		return Decimal(((F*D)/vd)*math.pow(math.e, -ke*tMax))

	def calculoCPO(F, D, ka, vd, ke):
		return Decimal((F*D*ka)/(vd*(ka-ke)))
			

if __name__ == '__main__': 
	
	# Classe para calculos
	c = ConcentracaoPlasmatica
	
	ka = 1.2
	# TAU = horas em horas
	J = 6
	F = 1
	# Dose do remedio
	D = 200
	cl = 2.4
	vd = 30

	ke = c.calculoKE(cl, vd)
	tp = c.calculoTP(ka, float(ke), J)
	rinf = c.rinf(float(ke), J)

	print (colored('Calculo da dosagem para janlea terapeutica', 'red'))	
	print (colored('Calculo de KE', 'blue'), '[', ke, ']')
	print (colored('Calculo de TP','blue'), '[', tp, ']')
	print (colored('Calculo de RINF', 'blue'), '[', rinf, ']')
	print (colored('Calculo de cpssMAX', 'blue'), '[', c.cpssMAX(F, D, float(rinf), vd, float(ke), float(tp)),']')
	print (colored('Calculo de cpssMIN', 'blue'), '[', c.cpssMIN(F, D, ka, float(ke), vd, float(rinf), J), ']')

	#----------------------------
	print (colored('\n\nAnalise de dados de concentracao Sanguinea', 'red'))

	# Dados para geracao do grafico
	tempo = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28]
	concentracao = [0, 23.7, 35.4, 39.6, 39.6, 37.2, 33.7, 29.7, 25.8, 22.1, 18.7, 15.8, 13.2, 11, 9.1]
	tempoExtr_Resi = [2, 4, 6, 8, 10, 12]
	extrapolada = [82, 68, 56, 49, 41, 34]
	residual = [58.3, 32.6, 16.4, 9.4, 3.8, 0.3]


	a = ACSanguinea

	# Parametros de calcalculoKE_KA(cf, ci, tf, ti)
	# cf = concentracaoFinal
	# ci = concentracaoInicial
	# tf = tempoFinal
	# ti = tempoInicial
	ke = a.calculoKE_KA(9.1, 13.2, 28, 24)
	ka = a.calculoKE_KA(0.3, 58.3, 12, 2)
	asct = a.asct(tempo, concentracao)
	ascExt = a.ascExt(concentracao[len(concentracao)-1], float(ke))
	ascInfinito = a.ascInfinito(asct, ascExt)
	tMax = a.tMax(abs(float(ka)), abs(float(ke)))
	calCL = a.calculoCL(0.8, 700, float(ascInfinito))
	calVD = a.calculoVD(float(calCL), float(abs(ke)))
	
	# Farmaco Absorvido, dosagem (mg), ke, tMax, calVD
	calMax = a.calculoCMAX(0.8, 700, float(abs(ke)), float(tMax), float(calVD))
	
	# Farmaco Absorvido, dosagem (mg), ka, calVD, ke
	calCP0 =  a.calculoCPO(0.8, 700, float(abs(ka)), float(calVD), float(abs(ke)))
	print ('Calculo ACSanguinea', colored('KE','blue'), '[', ke,']')
	print ('Calculo ACSanguinea', colored('KA', 'blue'), '[', ka,']')
	print ('Calculo ACSanguinea', colored('MeiaVida', 'blue'), '[', a.tempoMeiaVidaAbsorcao(float(ka)), ']')
	print ('Calculo ACSanguinea Verf Situaca Normal [', a.analiseSituacaoNormal(ka, ke), ']')
	print ('Calculo ACSanguinea asct [', asct, ']mg/mL.h')
	print ('Calculo ACSanguinea ascExt [', ascExt, ']mg/mL.h')
	print ('Calculo ACSanguinea ascInfinito [', ascInfinito, ']mg/mL.h')
	print ('Calculo ACSanguinea tMax [', tMax, ']h')
	print ('Calculo ACSanguinea calculoCL [', calCL, ']mg/L.h')
	print ('Calculo ACSanguinea calculoCL [', (calCL *1000) / 60, ']mL/min')
	print ('Calculo ACSanguinea calculoVD [', calVD,']L')
	print ('Calculo ACSanguinea calculoMax [', calMax, ']mg/L')
	print ('Calculo ACSanguinea calCP0 [', calCP0, ']mg/L')

	# Gerando Grafico
	# tempo, tempoExtr_Resi, concentracao, concentracaoExtra, concentracaoResidual
	# tempo -> array de tempo
	# tempoExtr_Resi -> array de tempo
	# concentracao -> array de concentracao do farmaco
	# concentracaoExtra -> array concentracaoExtrapolado, vem da geracao da reta extrapolada
	# concentracaoResidual -> array
	print (colored('Plotando o grafico', 'blue'))
	a.montarGrafico(tempo, tempoExtr_Resi, concentracao, extrapolada, residual)