import socket
import threading

bind_ip = "0.0.0.0"
bind_port = 9999

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((bind_ip, bind_port))
server.listen(5)

def atende_client(cliente):
	request = cliente.recv(1024)
	print("[*] Received: %s" % request)
	cliente.send(b"ACT!")
	cliente.close()

while True:
	client, addr = server.accept()
	print("[*] Accept connection from %s:%s" % (addr[0], addr[1]))
	client_handler = threading.Thread(target=atende_client, args=(client,))
	client_handler.start()