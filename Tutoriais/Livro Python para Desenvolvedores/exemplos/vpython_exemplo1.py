import visual

"""
Hexaedro
"""
#coordenadas para vertices e arestas
coords = (-3, 3)

#cor do vertice
cor1 = (0.9, 0.9, 1.0)

#cor da aresta
cor2 = (0.5, 0.5, 0.6)

#desenha esferas nos vertices
for x in coords:
	for y in coords:
		for z in coords:
			#pos eh a posicao do centro da esfera
			visual.sphere(pos=(x, y, z), color=cor1)

#desenha os cilindros das arestas
for x in coords:
	for z in coords:
		# pos eh a posicao do centro da base do cilindro
		# radius e o raio da base do cilindro
		# axis e o eixo do cilindro
		visual.cylinder(pos=(x, 3, z), color=cor2, radius=0.25, axis=(0, -6, 0))

	for y in coords:
		# pos eh a posicao do centro da base do cilindro
		# radius e o raio da base do cilindro
		# axis e o eixo do cilindro
		visual.cylinder(pos=(x, y, 3), color=cor2, radius=0.25, axis=(0, 0, -6))

for y in coords:
	for z in coords:
		visual.cylinder(pos=(3, y, z), color=cor2, radius=0.25, axis=(-6, 0, 0))