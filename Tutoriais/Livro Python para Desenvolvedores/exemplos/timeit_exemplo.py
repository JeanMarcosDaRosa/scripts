import timeit

cod1 = """
l = []
for i in range(100):
	l.append(i**2)
"""

print(timeit.Timer(cod1).timeit())

cod2 = 'list(x**2 for x in range(100))'

print(timeit.Timer(cod2).timeit())

cod3 = '[x**2 for x in range(100)]'

print(timeit.Timer(cod3).timeit())