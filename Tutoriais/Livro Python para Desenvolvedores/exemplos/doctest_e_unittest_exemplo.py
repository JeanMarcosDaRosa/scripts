import unittest
import doctest

def teste_doctest(x):
	"""
	Teste de doctest
	>>> teste_doctest(2)
	4
	>>> teste_doctest(3)
	9
	"""
	return x**2

class TestesDaAplicacao(unittest.TestCase):

	def setUp(self):
		self.seq = range(10)

	def test0(self):
		self.assertEqual(teste_doctest(2), 4)

	def test1(self):
		self.assertEqual(teste_doctest(3), 9)

	def test2(self):
		self.assertTrue(teste_doctest(2) is 4)


if __name__=='__main__':
	doctest.testmod()
	unittest.main()
