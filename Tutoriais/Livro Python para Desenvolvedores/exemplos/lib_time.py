import time

# localtime() Retorna a data e hora local no formato
# de uma estrutura chamada struct_time, que é uma
# coleção com os itens: ano, mês, dia, hora, minuto,
# segundo, dia da semana, dia do ano e horário de verão
print(time.localtime())

# retorna a data e a hora no formato string conforme
# a configuração do sistema operacional
print(time.asctime())

#retorna o tempo do sistema em segundos
print(time.time())

# gmtime converte os segundos em struct_time
print(time.gmtime(time.time()))

# mktime faz o inverso de gmtime, convertendo a struct_time
# em segundos
print(time.mktime(time.localtime()))

# clock retorna o tempo desde que o programa inicio
#time.sleep(2)
print(time.clock())

# sleep(X) faz o programa aguardar X segundos
for i in range(3):
	time.sleep(1)
	print("%d segundos" % i+1)