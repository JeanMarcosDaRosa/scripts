# modulo de plotagem de graficos
from visual.graph import *

# metodo de range com float
def drange(start, stop, step):
	s = start
	while s < stop:
		yield s
		s += step

# grafico de linhas simples
g1 = gcurve(color=(8.0, 6.0, 3.0))

# grafico de barras
g2 = gvbars(delta=0.02, color=(6.0, 4.0, 6.0))

#limites do eixo X do grafico
for x in drange(0., 10.1, .1):
	# plot() recebe X e Y
	# plotando a curva
	g1.plot(pos=(x, 3 * sin(x) + cos(5 * x)))

	#plotando as barras
	g2.plot(pos=(x, tan(x) * sin(4 * x)))
