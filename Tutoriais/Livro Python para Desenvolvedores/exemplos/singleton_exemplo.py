# Exemplo de classe sigleton
class Conexao():
	_instancia = None

	def __new__(cls):
		if cls._instancia == None:
			print('New')
			return super(Conexao, cls).__new__(cls)
		else:
			return cls._instancia

	def __init__(self):
		if Conexao._instancia == None:
			Conexao._instancia = self
			print('Init\n')
		else:
			print('Ja instanciado\n')


a = Conexao()
b = Conexao()
c = Conexao()