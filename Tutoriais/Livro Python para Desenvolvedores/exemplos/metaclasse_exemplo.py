import MySQLdb
import copy


class Singleton(type):

	def __init__(cls, name, bases, dic):
		type.__init__(cls, name, bases, dic)

		def __copy__(self):
			return self

		def __deepcopy__(self, memo=None):
			return self

		cls.__copy__ = __copy__
		cls.__deepcopy__ = __deepcopy__

	def __call__(cls, *args, **kargs):
		try:
			return cls.__instance
		except AttributeError as e:
			cls.__instance = super(Singleton, cls).__call__(*args, **kargs)
			return cls.__instance


class Conexao(object, metaclass=Singleton):

	

	def __init__(self):
		con = MySQLdb.connect(host='192.168.0.105', user='root', passwd='nobug95')
		self.db = con.cursor()


class Log(object):

	def __init__(self):
		self.log = open('msg.log', 'w')

	def write(self, msg):
		print(msg)
		self.log.write(str(msg)+'\n')



# testar pq nao funciona
con1 = Conexao()
Log().write('con1 id=%d' % id(con1))
con1.db.execute('show processlist')
Log().write(con1.db.fetchall())

con2 = Conexao()
Log().write('con2 id=%d' % id(con2))
con2.db.execute('show processlist')
Log().write(con2.db.fetchall())

con3 = copy.copy(con1)
Log().write('con3 id=%d' % id(con3))
con3.db.execute('show processlist')
Log().write(con3.db.fetchall())

