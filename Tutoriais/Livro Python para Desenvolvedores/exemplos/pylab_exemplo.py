from pylab import *
import numpy as np
import matplotlib.pyplot as plt

ent = np.arange(0., 20.1, .1)

sai = np.cos(ent)

#plota a curva

plt.plot(ent, sai)

#texto para o eixo x
plt.xlabel('entrada')

#texto para o eixo y
plt.ylabel('cosseno')

plt.title('Cossenos')

#ativa a grade
plt.grid(True)

plt.show()
