
class Tribonacci2():

	def __tribonacci_gen(self):
	    a, b, c = 0, 0, 1
	    while 1:
	        yield c
	        a, b, c = b, c, a + b + c

	def set_numero(self, n):
		self.n = n

	def run_tribonacci(self):
		it = self.__tribonacci_gen()
		return [next(it) for x in range(self.n)]

	resultado = property(run_tribonacci, set_numero)


if __name__ == '__main__':
	t2 = Tribonacci2()
	t2.resultado = 10
	print(t2.resultado)