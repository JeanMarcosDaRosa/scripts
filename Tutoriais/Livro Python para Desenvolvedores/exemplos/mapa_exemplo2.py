from mpl_toolkits.basemap import Basemap
from matplotlib import pyplot
mapa = Basemap(projection='ortho', lat_0=10, lon_0=-10, resolution='l', area_thresh=1e3)
# Preenche o mapa com relevo
mapa.bluemarble()
mapa.drawmapboundary()
lxy = (('Rio\nde\nJaneiro', -43.11, -22.54), ('Londres', 0.07, 50.30))
# Transposta
lxy = zip(*lxy)
# Converte as coordenadas
x, y = mapa(lxy[1], lxy[2])
lxy = lxy[0], x, y
# Marca no mapa
mapa.plot(x, y, 'w^')
# Escreve os nomes
for l, x, y in zip(*lxy):
	pyplot.text(x+2e5, y-6e5, l,color='#eeeecc')
pyplot.savefig('mapa2.png', dpi=150)