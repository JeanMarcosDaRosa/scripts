from numba import jit
from decorador import this_time


@jit
def cod1():
	l = []
	for i in range(1000):
		l.append(i**2)
	print(l)

@jit
def cod2():
	s = list(x**2 for x in range(100))

@jit
def cod3():
	s = [x**2 for x in range(100)]

@this_time
def test():
	#Nao suporta geradores e list comprehenssions
	#cod1()
	cod2()
	#cod3()

test()

#print(timeit.Timer('cod1()').timeit())
#print(timeit.Timer('cod2()').timeit())
#print(timeit.Timer('cod3()').timeit())
