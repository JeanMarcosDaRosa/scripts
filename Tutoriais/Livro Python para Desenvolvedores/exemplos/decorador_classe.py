def show_args(cls):
	
	class Show(cls):

		def __init__(self, *args, **kargs):
			print('Argumentos:', *args)
			print('Argumentos dicionario:', kargs)
			cls.__init__(self, *args, **kargs)

	return Show

@show_args
class Teste(object):

	def __init__(self, x, lista=None):
		self.x = x
		self.lista = lista

t = Teste(1, lista=[1, 2, 3])