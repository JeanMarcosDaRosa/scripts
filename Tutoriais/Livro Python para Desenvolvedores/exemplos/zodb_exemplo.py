from ZODB import FileStorage, DB
import transaction

db = FileStorage.FileStorage('banco.fs')
con = DB(db).open()

root = con.root()

root['nome'] = 'jean marcos'
root['empresa'] = {'nome': 'tn3', 'cidade': 'Passo Fundo'}

transaction.commit()
print(root['nome'])
root['nome'] = 'marcos jean'
print(root['nome'])
transaction.abort()
print(root['nome'])


