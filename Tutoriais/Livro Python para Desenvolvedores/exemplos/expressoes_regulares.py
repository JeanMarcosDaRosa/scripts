import re

string = 'Jean Marcos da Rosa, e (54) 99261407'

# compila a expressao regular
rec = re.compile('\w+')

# Encontra todas as ocorrências que atendam a expressão
print(rec.findall(string))

# enconcontrando...
rec = re.compile('[Jj]ean')
for x in string.split(' '):
	print(x, bool(rec.match(x)))

# substituindo...
print(re.sub('an', 'AN', string))

# separando
print(re.split(',?\s+e?\s+', string))
