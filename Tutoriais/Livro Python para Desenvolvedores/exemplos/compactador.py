import zipfile
import glob

def zippar(arquivo, lista_arq=None, lista_str=None):
	z = zipfile.ZipFile('arq.zip', 'w', zipfile.ZIP_DEFLATED)
	if lista_arq != None:
		for x in lista_arq:
			z.write(x)
	if lista_str!=None:
		for i, x in enumerate(lista_str):
			z.writestr(str(i)+'.txt', x)
	z.close()

def dezippar(arquivo):
	z = zipfile.ZipFile(arquivo)
	for x in z.namelist():
		info = z.getinfo(x)
		print("Tamanho original:",info.file_size)
		print("Tamanho compactado:",info.compress_size)
		print(z.read(x))
#zippar('arq.zip', [str(x) for x in glob.glob('*.*')])

#dezippar('arq.zip')