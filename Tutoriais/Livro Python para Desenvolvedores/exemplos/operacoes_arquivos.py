import os

def listar_arquivos(path='.'):
	for f in os.listdir(path):
		fn = os.path.normpath(os.path.join(path, f))
		if os.path.isdir(fn):
			for i in listar_arquivos(fn):
				yield i
		else:
			yield fn

for a in listar_arquivos('../'):
	print(a)