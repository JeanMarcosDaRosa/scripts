from abc import ABCMeta, abstractmethod

class ExemploABC(object, metaclass=ABCMeta):

	@abstractmethod
	def ex1(self, x):
		pass

class SubClasse(ExemploABC):
	#deve-se implementar o metodo ex1, senão vai haver erro
	pass

s = SubClasse()

