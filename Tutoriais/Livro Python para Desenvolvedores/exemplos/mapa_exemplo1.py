# http://ilessendata.blogspot.com.br/2014/02/making-maps-python.html
from mpl_toolkits.basemap import Basemap
from matplotlib import pyplot
from numpy import arange

# Cria um mapa usando basemap
mapa = Basemap(projection='robin', lat_0=-20, lon_0=-50, resolution='1', area_thresh=1e3)

# Desenha a costa dos continentes
mapa.drawcoastlines(color='#777799')

# Desenha as fronteiras
mapa.drawcountries(color='#ccccee')

# Pinta os continentes
mapa.fillcontinets(color='#ddddcc')

# Desenha os meridianos
mapa.drawmeridians(arange(0, 360, 30), color='#ccccee')

# Desenha os paralelos
mapa.drawparallels(arange(-180, 180, 30), color='#ccccee')

# Desenha os limites do mapa
mapa.drawmapboundary()

# Salva a imagem
pyplot.savefig('mapa1.png', dpi=150)