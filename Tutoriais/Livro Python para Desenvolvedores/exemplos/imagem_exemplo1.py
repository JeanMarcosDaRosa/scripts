import glob
from PIL import Image, ImageFilter

for fn in glob.glob('*.jpg'):
	n = glob.os.path.splitext(fn)[0]
	print('Processando %s' % n)
	img = Image.open(fn)
	img.thumbnail((256, 256), Image.ANTIALIAS)
	img = img.filter(ImageFilter.SMOOTH)
	img.save(n+'.png', 'PNG')


