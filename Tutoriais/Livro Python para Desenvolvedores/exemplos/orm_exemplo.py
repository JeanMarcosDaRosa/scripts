from sqlalchemy import *

#em memoria, senao podemos armazenar com sqlite:///arquivo.db
db = create_engine('sqlite://')

#torna os metadados acessiveis
metadata = MetaData(db)
# log os sqls gerados
metadata.bind.echo = True

tabela = Table('programas', metadata, 
	Column('id', Integer, primary_key=True), Column('nome', String(60)))

#cria a tabela no banco
tabela.create()

#recarrega a tabela
tabela = Table('programas', metadata, autoload=True)

#insere com dicionarios, nao precisa do id
tabela.insert().execute({'nome': 'Firefox'}, {'nome': 'GIMP'}, {'nome': 'Excel'})

sel = tabela.select()
r = sel.execute()

for x in r.fetchall():
	print(x)