from PIL import Image, ImageDraw

l, a = 512, 512

#cria uma imagem com o fundo branco
imagem = Image.new('RGBA', (l, a), 'white')

#objeto de desenho age sobre objeto de imagem
desenho = ImageDraw.Draw(imagem)

# calcula a largura da faixa de cor
faixa = l / 256

for i in range(l):
	rgb = (0.25 * i / faixa, 0.5 * i / faixa, i / faixa)
	cor = '#%02x%02x%02x' % rgb
	desenho.line((0, i, l, i), fill=cor)

#copia e cola recortes invertidos do gradiente
for i in range(l, l // 2, -l // 10):
	#tamanho do recorte
	area = (l-i, a-i, i, i)

	#copia e inverte
	recorte = imagem.crop(area).transpose(Image.FLIP_TOP_BOTTOM)

	#cola devolta na imagem original
	imagem.paste(recorte, area)

imagem.save('teste.png', 'PNG')