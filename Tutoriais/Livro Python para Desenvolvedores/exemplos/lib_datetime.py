import datetime

# datetime cria uma data, parametro yyyy, MM, dd, hh, mm, ss
print(datetime.datetime(2015, 7, 30, 13, 7, 0))

dt = datetime.datetime(2015, 7, 30, 13, 7, 0)

# date mostra a data do datetime
print(dt.date())

# time mostra as horas, minutos e segundos daquela datetime
print(dt.time())

# today mostra a data e hora atual do sistema
print(dt.today())

# quanto tempo falta para 02/01/2015 ??
dt = datetime.datetime(2016, 1, 2, 18, 0, 0)
print(dt - dt.today())
