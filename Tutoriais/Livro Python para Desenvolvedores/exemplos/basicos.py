__author__ = 'jean.marcos'
import os
import sys
import platform
import string
import glob
import cmath
import math
import string
import tempfile
import random
from fractions import Fraction, gcd


print('String Template')
st = string.Template("O numero eh $num1 para $num2")
s = st.substitute({'num1': 1, 'num2': 4})
print(s)
print('Enumerate')
l = ['jean', 'marcos', 'da', 'rosa']
for i, n in enumerate(l):
    print(i, n)
print('Sets')
s1 = set(range(5))
s2 = set(range(3, 9))

print(s2.union(s1))
print(s2.difference(s1))
print(s2.intersection(s1))
print(s2.issuperset(s1))
print(s2.isdisjoint(s1))

print('Matriz Esparsa')
dim = 8, 6
matriz = {}
matriz[1, 3] = 1
matriz[3, 4] = 1
matriz[1, 2] = 1
matriz[5, 5] = 1
matriz[4, 2] = 1
matriz[8, 3] = 1

for i in range(dim[0]):
    for j in range(dim[1]):
        print(matriz.get((i, j), 0), end=' ')
    print()

#print("Ordenacao")
#def _cmp(x, y):
#    return cmp(x[-1], y[-1])


#dados = [(4, 3), (5, 1), (7, 2), (9, 0)]
#print(sorted(dados, _cmp))

print('Glob')
for i in glob.glob('*.py'):
    print(i)

#Continuar pagina 69

print("Numeros complexos")

for c in [2j, 3j, 3 * 4j]:
	print(c)
	print("Polar:", cmath.polar(c))
	print("Amplitude:", abs(c))
	print("Angulo:", math.degrees(cmath.polar(c)[0]))


print("Numeros aleatorios")

#escolha entre uma lista
print(random.choice(range(11)))
print(random.choice(string.ascii_uppercase))

#escolha com range
print(random.randrange(1, 10))

#escolha entre 0 e 1
print(random.random())


print(Fraction('.2'))
print(Fraction(3, 4))

#maior divisor comum
print(gcd(45, 78))


print("Arquivos")

sys.stdout.write("olha que legal isso\n")

print("existe" if os.path.exists('basicos.py') else "nao existe")

for i, l in enumerate(open('basicos.py', 'r')):
	print(i+1, ' ', l)

temp = tempfile.TemporaryFile()
temp.write(b"Jean Marcos da Rosa")
temp.seek(0)
temp.read()
temp.close()

print("Sistema Operacional")

def uid():
	s = {'Windows': 'USERNAME', 'Linux': 'User'}
	return os.environ.get(s[platform.system()])

print(uid())
print('Diretorio corrent', os.path.abspath(os.curdir))
print(os.path.split(sys.executable))
print(os.path.split('/aaa/sea.ss'))
print(os.path.splitext('/aaa/sea.ss'))

print('Introspeccao')

class Obj1(list):

	def __init__(self, var1):
		self.var1 = var1

	def metodo(self, var1):
		self.var1 = var1


o = Obj1('Jean')

# lista os atributos do objeto
print(vars(o))

o.metodo('Teste')

print(vars(o))

# estrutura do objeto
print(dir(o))

# representacao do objeto
print(repr(o))

# variaveis locais
print(locals())

# variaveis globais
print(globals())

print(isinstance(o, Obj1))

print(issubclass(Obj1, list))

print("Funcional")

x = [1, 2, 3, 4, 5, 6]

# opera o calculo sobre todos os valores, gerando outra lista
print(list(map(lambda x: x**2, x)))

# filtra
print(list(filter(lambda x: x % 2 == 0, x)))

# soma simples
print(sum(x))

# math traz uma funcao que gera o fatorial
print(math.factorial(5))

# transposicao ou agrupamento de sequancias com zip
y = [6, 5, 4, 3, 2, 1]
print(list(zip(x, y)))
