import time

def this_time(func):
	def temporizador(*args, **kwargs):
		antes = time.time()
		result = func(*args, **kwargs)
		print("Levou %d segundos" % (time.time()-antes))
		return result
	return temporizador

if __name__ == '__main__':
	@this_time
	def teste(iteracoes):
		for i in range(iteracoes):
			time.sleep(1)
			print('Iter:', i)

	teste(5)