from visual import *

# Cores
azul = (0.25, 0.25, 0.50)
verde = (0.25, 0.50, 0.25)

# Eixo de rotacao
eixo = (0, 1, 0)

# cria o frame alinhado com o eixo de rotacao
fr = frame(axis=eixo)

# o fundo da caixa
box(pos=(0, -0.5, 0), color=azul, size=(10.0, 0.5, 8.0))

# O bordas da caixa
box(pos=(0, -0.5, 4.0), color=azul, size=(11.0, 1.0, 1.0))
box(pos=(0, -0.5, -4.0), color=azul, size=(11.0, 1.0, 1.0))
box(pos=(5.0, -0.5, 0), color=azul, size=(1.0, 1.0, 8.0))
box(pos=(-5.0, -0.5, 0), color=azul, size=(1.0, 1.0, 8.0))

#o piao
py1 = pyramid(frame=fr, pos=(1, 0, 0), color=verde, axis=(1, 0, 0))
py2 = pyramid(frame=fr, pos=(1, 0, 0), color=verde, axis=(-1, 0, 0))

#o piao anda no plano y = 0
delta_x = 0.01
delta_z = 0.01

while True:
	#inverte o sentido em X
	if abs(fr.x) > 3.1:
		delta_x = -delta_x

	#inverte o sentido em Z
	if abs(fr.z) > 3.1:
		delta_z = -delta_z

	fr.x += delta_x
	fr.z += delta_z

	#rotaciona em PI / 100 no eixo
	fr.rotate(angle=pi/100, axis=eixo)

	#espere 1/100 segundos
	rate(250)