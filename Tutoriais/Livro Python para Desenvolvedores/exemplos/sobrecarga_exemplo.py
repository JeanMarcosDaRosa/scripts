# exemplo de sobrecarga de operadores
import random

class Int(int):
	
	def __add__(self, *args):
		return random.randrange(1, 10)

print(Int(10) + Int(5))
