import os
import time
import threading

class Monitor(threading.Thread):

	def __init__(self, ip):
		self.ip = ip
		self.status = None
		threading.Thread.__init__(self)

	def run(self):
		ret = os.popen('ping -n 1 %s' % self.ip).read()

		if 'Esgotado' in ret:
			self.status = False
		else:
			self.status = True

monitores=[]

for i in range(100, 111):
	monitores.append(Monitor('192.168.0.%s' % str(i)))

for m in monitores:
	m.start()

while True:
	termina = True
	for m in monitores:
		if m.status == None:
			termina = False
			break
	if termina:
		break
	time.sleep(1)

for m in monitores:
	print(m.ip, ', status:', 'ativo' if m.status else 'inativo')
