#coding utf-8
import os
import glob

def split(fn, n=100):
	try:
		pasta = os.path.dirname(fn)
		n1, n2 = os.path.splitext(fn)
		n1 = os.path.splitext(os.path.basename(n1))[0]
		cont = 0
		with open(fn, 'r', encoding='latin-1') as f:
			i = 0
			g = open(pasta+'/'+n1+"_{0:03d}".format(i)+n2, 'w', encoding='latin-1')
			for s in f:
				g.write(s)
				cont+=1
				if cont >= n:
					g.close()
					cont = 0
					g = open(pasta+'/'+n1+"_{0:03d}".format(i)+n2, 'w', encoding='latin-1')
					i+=1
			if cont > 0:
				g.close()
		f.close()
	except Exception as e:
		print(e)
		return None

split('C:/Users/jean.marcos/Documents/dumps/cep_ciot.sql', 1000)
