import cherrypy
from datetime import datetime

class Root(object):

	@cherrypy.expose
	def index(self):
		hj = datetime.now()
		if hj.hour > 6 and hj.hour < 12:
			return 'Bom dia, sao %s:%s' % (hj.hour, hj.minute)
		elif hj.hour >= 12 and hj.hour < 19:
			return 'Boa tarde, sao %s:%s' % (hj.hour, hj.minute)
		else:
			return 'Boa noite, sao %s:%s' % (hj.hour, hj.minute)

cherrypy.quickstart(Root())
