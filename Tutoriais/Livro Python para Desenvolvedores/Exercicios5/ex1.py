from xml.etree.ElementTree import Element, ElementTree
import os

class Animal():

	def __init__(self, nome, especie, genero, peso, altura, idade):
		self.nome = nome
		self.especie = especie
		self.genero = genero
		self.peso = peso
		self.altura = altura
		self.idade = idade

	def salvar(self):
		animal = Element('Animal', nome=self.nome, especie=str(self.especie), genero=str(self.genero), peso=str(self.peso), altura=str(self.altura), idade=str(self.idade))
		if os.path.exists('animais.xml') and os.path.isfile('animais.xml'):
			try:
				root = ElementTree(file='animais.xml').getroot()
			except:
				root = Element('Animais')
		else:
			root = Element('Animais')
		try:
			for sroot in root.getchildren():
				found = sroot.find("[@nome='"+self.nome+"']")
				if found!=None:
					root.remove(found)
			root.append(animal)
			ElementTree(root).write('animais.xml')
		except Exception as e:
			print('Falha ao salvar animal: %s' % (self.nome))
			print(e)

	def desfazer(self):
		if os.path.exists('animais.xml') and os.path.isfile('animais.xml'):
			root = ElementTree(file='animais.xml').getroot()
			for sroot in root.getchildren():
				found = sroot.find("[@nome='"+self.nome+"']")
				if found!=None:
					for x in found.attrib:
						setattr(self, x, found.attrib[x])
		else:
			print('Objeto nao encontrado')


	def __repr__(self):
		return "Nome: %s, Especie: %s, Genero: %s, Peso: %.2f, Altura: %.2f, Idade: %d" % (self.nome, self.especie, self.genero, float(self.peso), float(self.altura), int(self.idade))


lista_animais = []

cachorro1 = Animal('Billy', 'Cachorro', 'Macho', 1.2, 0.20, 1)
cachorro2 = Animal('Kimberly', 'Cachorro', 'Femea', 1.7, 0.30, 3)
periquito = Animal('Appolo', 'Periquito', 'Macho', 0.1, 0.07, 1)

lista_animais.append(cachorro1)
lista_animais.append(cachorro2)
lista_animais.append(periquito)


for animal in lista_animais:
	animal.salvar()

print("Recuperando...")
for animal in lista_animais:
	animal.desfazer()
	print(animal)

#ler do xml e printar
#cachorro2.find(".//*[@nome='"+cachorro2.nome+"']")