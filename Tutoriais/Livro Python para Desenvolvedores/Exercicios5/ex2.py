def list_tuple_to_table(dados):
	table = ''
	for x in dados:
		dt = ''
		for k in x:
			dt += '\n\t\t<td>%s</td>' % str(k)
		table+= '\n\t<tr>%s\n\t</tr>' % dt
	return '<table>%s\n</table>' % (table)

dados = [(1, 2, 3), (4, 5, 6), (4, 5, 6), (4, 5, 6), (4, 5, 6), (4, 5, 6)]
print(list_tuple_to_table(dados))