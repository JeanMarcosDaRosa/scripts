import glob as g
import os

def diff(pasta1, pasta2):
	p1 = set(sorted([os.path.basename(a) for a in g.glob(pasta1+'*')]))
	p2 = set(sorted([os.path.basename(a) for a in g.glob(pasta2+'*')]))
	for p in p1:
		if p not in p2:
			print(pasta1+p)
		else:
			s1 = open(pasta1+p).readlines()
			s2 = open(pasta2+p).readlines()
			if s1 != s2:
				print(pasta1+p)
				print(pasta2+p)

	for p in p2:
		if p not in p1:
			print(pasta2+p)

	#print(p1.difference(p2).union(p2.difference(p1)))

diff('C:\\tmp\\Linux1\\', 'C:\\tmp\\Linux2\\')