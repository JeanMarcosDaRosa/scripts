def le_arquivo(fn):
	try:
		result = []
		with open(fn, 'r') as f:
			for x in f:
				result.append(tuple(x.split(',')))
		return result
	except Exception as e:
		print(e)
		raise SystemExit

print(le_arquivo('ex3.py'))