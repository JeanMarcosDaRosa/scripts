import os
import glob
def split(fn, n=100):
	try:
		pasta = os.path.dirname(fn)
		n1, n2 = os.path.splitext(fn)
		with open(fn, 'r') as f:
			i = 0
			while True:
				with open(pasta+n1+"_{0:03d}".format(i)+n2, 'w') as g:
					s = f.read(n)
					g.write(s)
					i+=1
					if len(s) < n: break
				g.close()
				if i >= 30: break
		f.close()
	except Exception as e:
		print(e)
		return None

#def join(fn, fnlist):

#split('ex4.py', 40)

def join(fn, fnlist):
	try:
		with open(fn, 'a') as fw:
			for a in fnlist:
				fr = open(a, 'r')
				fw.write(fr.read())
				fr.close()
		fw.close()
	except Exception as e:
		print(e)
		return None

lista = [str(k) for k in glob.glob('*.py')]
join('result_join.py', lista)