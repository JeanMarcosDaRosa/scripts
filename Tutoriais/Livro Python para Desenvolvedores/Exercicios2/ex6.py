def count(fn):
	try:
		dici = {}
		with open(fn, 'r') as f:
			for x in f:
				for p in x.replace('\n', '').replace('\t', '').split(' '):
					if p in dici:
						dici[p] += 1
					else:
						dici[p] = 1
		return sorted(dici.items(), key=lambda x: x[1])
	except Exception as e:
		print(e)
		return None


print(count('ex6.py'))