def estatisticas(fn):
	chars, linhas, palavras = 0, 0, 0
	with open(fn, "r") as f:
		for l in f:
			linhas += 1
			chars += len(l)
			palavras += len(l.split(' '))

	return {'caracteres': chars, 'linhas': linhas, 'palavras': palavras}

print(estatisticas('ex1.py'))