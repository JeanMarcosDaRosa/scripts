
def matrix_sum(*matrices):
	m1, m2 = matrices[0], matrices[1]
	total = 0
	for i in range(len(m1)):
		for j in range(len(m1[i])):
			total += m1[i][j] + m2[i][j]
	return total


def camel_case(valor):
	novo = ''
	for x in valor.split('_'):
		novo += x[0].upper()+x[1:]
	return novo


mat1 = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
mat2 = [[1, 1, 1],[2, 2, 2],[3, 3, 3]]
print(matrix_sum(mat1, mat2))

print(camel_case("nome_da_variavel_x"))