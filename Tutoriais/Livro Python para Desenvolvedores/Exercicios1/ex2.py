import math
def eh_primo(numero):
	s = int(math.sqrt(numero))
	for i in range(2, s+1):
		if numero % i == 0:
			return False
	return True

for x in range(101):
	print(eh_primo(x))
