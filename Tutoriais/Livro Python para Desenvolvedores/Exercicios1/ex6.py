__autor__ = 'jean'

def ordena(dados, chave=0, reverso=False):
	""" 
	Teste de documentacao com PyDOC
	"""
	return sorted(dados, key=lambda x: x[chave], reverse=reverso)

if __name__=='__main__':
	dados = [(1, 6), (2, 5), (3, 4), (4, 3), (5, 2), (6, 1)]
	print(ordena(dados, chave=0))