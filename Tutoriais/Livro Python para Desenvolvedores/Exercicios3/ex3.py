import cProfile

def rgb():
	for r in range(256):
		for g in range(256):
			for b in range(256):
				yield r, g, b

def teste():
	it = rgb()
	for x in range(10):
		print(next(it))

cProfile.run('teste()')