import itertools

def eh_primo(val):
	eh_primo = True
	for x in range(1, (val//2)+1):
		if 1 < x < i and i % x == 0:
			eh_primo = False
			break
	return eh_primo

primos = (x for x in itertools.count() if eh_primo(x))

for i in range(100):
	print(next(primos))