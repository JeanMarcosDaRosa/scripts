
def numeros_primos(maximo=100):
	for i in range(maximo):
		eh_primo = True
		for x in range(1, i):
			if 1 < x < i and i % x == 0:
				eh_primo = False
				break
		if eh_primo: yield i

for i in numeros_primos():
	print(i)