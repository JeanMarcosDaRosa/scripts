
def le_arquivo(fn):
	try:
		with open(fn, 'r') as f:
			for x in f.readlines():
				yield tuple(x.split(','))
		f.close()
	except Exception as e:
		print(e)
		exit(1) 

for x in le_arquivo('ex4.py'):
	print(x)