
class Tribonacci():


	def __tribonacci_gen(self):
	    a, b, c = 0, 0, 1
	    while 1:
	        yield c
	        a, b, c = b, c, a + b + c

	def run_tribonacci(self, n):
		it = self.__tribonacci_gen()
		return [next(it) for x in range(n)]

if __name__ == '__main__':
	t = Tribonacci()
	print(t.run_tribonacci(10))


