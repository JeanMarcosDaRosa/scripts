class MyList(list):

	def __init__(self, *args):
		super().__init__(args)

	def allElements(self):
		return list(set(self))


m = MyList(1, 2, 3, 3, 2, 6)
print(m.allElements())