class Vetor():

	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

	def __repr__(self):
		return 'Vetor(%.2f, %.2f, %.2f)' % (self.x, self.y, self.z)

	def __add__(self, vetor):
		self.x += vetor.x
		self.y += vetor.y
		self.z += vetor.z
		return self

	def __sub__(self, vetor):
		self.x -= vetor.x
		self.y -= vetor.y
		self.z -= vetor.z
		return self

	def __mul__(self, vetor):
		self.x *= vetor.x
		self.y *= vetor.y
		self.z *= vetor.z
		return self

	def __abs__(self):
		return self.x**2+self.y**2+self.z**2

v1 = Vetor(1, 2, 3)
v2 = Vetor(1.5, 2.5, 3.5)
print(abs(v1))
print(v1 + v2)