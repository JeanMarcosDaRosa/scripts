class Quadrado():
	
	@classmethod
	def __init__(self, lado=0):
		self.lado = lado

	@classmethod
	def muda_valor_lado(self, lado):
		self.lado = lado

	@classmethod
	def retornar_valor_lado(self):
		return self.lado

	@classmethod
	def calcular_area(self):
		return self.lado * self.lado

q = Quadrado(3)
print("Area:", q.calcular_area())
q.muda_valor_lado(2)
print("Area:", q.calcular_area())
print(q.retornar_valor_lado())
