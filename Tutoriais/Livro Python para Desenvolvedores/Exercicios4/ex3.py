class Carro():

	def __init__(self, consumo):
		self.consumo = consumo
		self.combustivel = 0

	def mover(self, km):
		gasto = (km / self.consumo)
		if gasto <= self.combustivel:
			self.combustivel -= gasto
		else:
			self.combustivel = 0

	def gasolina(self):
		return self.combustivel

	def abastecer(self, litros):
		self.combustivel += litros

fusion = Carro(11)
fusion.abastecer(100)
print("%.2f litros no tanque." % fusion.gasolina())
fusion.mover(215)
print("%.2f litros no tanque." % fusion.gasolina())
