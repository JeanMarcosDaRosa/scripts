import win32print
import win32api
import sys
import subprocess

class OurList(list):
    def join(self, s):
        return s.join(self)


def build_dict():
    lst = win32print.EnumPrinters(win32print.PRINTER_ENUM_CONNECTIONS + win32print.PRINTER_ENUM_LOCAL)
    prdict = {}
    for flags, description, name, comment in lst:
        prdict[name] = {}
        prdict[name]["flags"] = flags
        prdict[name]["description"] = description
        prdict[name]["comment"] = comment
    return prdict


def default():
    return win32print.GetDefaultPrinter()


def listprinters():
    lista = []
    prdict = build_dict()
    for k, y in prdict.items():
        lista.append(k)
    return lista


def printdocfile(filename, printer, gsprint):
    try:
        #gsprint.exe -ghostscript gswin32c.exe -printer -noquery -mono
        #win32api.ShellExecute(0, 'open', gsprint, '-printer "' + printer + '" ' + filename, '.', 0)
        #win32api.ShellExecute(0, 'open', gsprint, ' -q -dNOTRANSPARENCY -dDOINTERPOLATE -dNOPAUSE -dBATCH -dGraphicsAlphaBits=4 -sDEVICE=png16m -dTextAlphaBits=4 -dEPSCrop -noquery -landscape -printer "' + printer + '" ' + filename, '.', 0)#-sCompression=lzw -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen  -dDetectDuplicateImages=true
        #win32api.ShellExecute(0, 'open', gsprint, ' -dAutoRotatePages=/All -dNOPAUSE -dBATCH -sPAPERSIZE=a4 -dFIXEDMEDIA -dPDFFitPage -dEmbedAllFonts=true -dSubsetFonts=true -dPDFSETTINGS=/prepress -dNOPLATFONTS -sFONTPATH=\"C:\\Windows\\Fonts" -noquery -dNumCopies=1 -all -colour -printer "' + printer + '" ' + filename, '.', 0)
        #win32api.ShellExecute (0,"print",filename,'/d:"%s"' % printer,".",0)
        #acrobatexe = "C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe"
        #subprocess.call([acrobatexe, "/n", "/o", "/s", "/t", filename, printer])

        win32api.ShellExecute(0, 'open', gsprint, '-sCompression=lzw -dNOTRANSPARENCY -dDOINTERPOLATE -dPDFSETTINGS=/screen -noquery -printer "' + printer + '" ' + filename, '.', 0)#-sCompression=lzw -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen  -dDetectDuplicateImages=true
        print('OK')
    except Exception as e:
        print(e)


if len(sys.argv) > 1:
    if sys.argv[1] == 'listprinters':
        print(OurList(listprinters()).join(","))
    elif sys.argv[1] == 'print':
        printdocfile(sys.argv[2], sys.argv[3], sys.argv[4])  # caminho do arquivo, nome da impressora
    elif sys.argv[1] == 'default':
        print(default())