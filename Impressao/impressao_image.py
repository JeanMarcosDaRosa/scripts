import win32print
import win32ui
from PIL import Image, ImageWin, ImageFilter
import sys
from win32api import *
try:
    from winxpgui import *
except ImportError:
    from win32gui import *
from win32gui_struct import *
import win32con
import struct

class OurList(list):
    def join(self, s):
        return s.join(self)


def build_dict():
    lst = win32print.EnumPrinters(win32print.PRINTER_ENUM_CONNECTIONS + win32print.PRINTER_ENUM_LOCAL)
    prdict = {}
    for flags, description, name, comment in lst:
        prdict[name] = {}
        prdict[name]["flags"] = flags
        prdict[name]["description"] = description
        prdict[name]["comment"] = comment
    return prdict


def default():
    return win32print.GetDefaultPrinter()


def listprinters():
    lista = []
    prdict = build_dict()
    for k, y in prdict.items():
        lista.append(k)
    return lista


def printdocfile(filename, printer):
	try:
		# HORZRES / VERTRES = printable area
		HORZRES = 8
		VERTRES = 10
		# LOGPIXELS = dots per inch
		LOGPIXELSX = 88
		LOGPIXELSY = 90
		# PHYSICALWIDTH/HEIGHT = total area
		PHYSICALWIDTH = 110
		PHYSICALHEIGHT = 111
		# PHYSICALOFFSETX/Y = left / top margin
		PHYSICALOFFSETX = 112
		PHYSICALOFFSETY = 113

		hDC = win32ui.CreateDC()
		#SetBkColor(hDC, 0x0)
		hDC.CreatePrinterDC(printer)
		#hDC.SetMapMode(win32con.MM_TWIPS)
		printable_area = hDC.GetDeviceCaps(HORZRES), hDC.GetDeviceCaps(VERTRES)
		printer_size = hDC.GetDeviceCaps(PHYSICALWIDTH), hDC.GetDeviceCaps(PHYSICALHEIGHT)
		printer_margins = hDC.GetDeviceCaps(PHYSICALOFFSETX), hDC.GetDeviceCaps(PHYSICALOFFSETY)

		bmp = Image.open(filename)
		#if bmp.size[0] > bmp.size[1]:
		#	bmp = bmp.rotate(90)#Image.ANTIALIAS
		#bmp = bmp.filter(ImageFilter.GaussianBlur(radius=1))
		ratios = [1.0 * printable_area[0] / bmp.size[0], 1.0 * printable_area[1] / bmp.size[1]]
		scale = min(ratios)

		hDC.StartDoc(filename)
		hDC.StartPage()

		dib = ImageWin.Dib(bmp)
		scaled_width, scaled_height = [int(scale * i) for i in bmp.size]
		x1 = int((printer_size[0] - scaled_width) / 2)
		y1 = int((printer_size[1] - scaled_height) / 2)
		x2 = x1 + scaled_width
		y2 = y1 + scaled_height
		dib.draw(hDC.GetHandleOutput(),(x1, y1, x2, y2))

		hDC.EndPage()
		hDC.EndDoc()
		hDC.DeleteDC()
		print('OK')
	except Exception as e:
		print(e)


if len(sys.argv) > 1:
    if sys.argv[1] == 'listprinters':
        print(OurList(listprinters()).join(","))
    elif sys.argv[1] == 'print':
        printdocfile(sys.argv[2], sys.argv[3])  # caminho do arquivo, nome da impressora
    elif sys.argv[1] == 'default':
        print(default())

#print(OurList(listprinters()).join(","))
printdocfile('test.png', 'PDFCreator') #'HP LaserJet 200 color MFP M276 PCL6 Class Driver', 'PDFCreator'