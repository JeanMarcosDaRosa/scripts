import sublime, sublime_plugin, sys
sys.path.append('C:\\Python34\\Lib\\site-packages')
import openpyxl

class NewXlsxFileCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		my_file = self.view.file_name()
		if my_file.endswith('.xlsx'):
			planilhas = get_nome_planilhas(my_file)
			for p in planilhas:
				cmd = self.view.window().new_file()
				cmd.set_name(p)
				conteudo = p+'\n'
				for x in gen_linhas_xlsx(my_file, planilha=p):
					conteudo += '\t'.join(str(v) for v in x)+'\n'
				cmd.insert(edit,0, str(conteudo))


def get_nome_planilhas(xlsx):
	if type(xlsx) == openpyxl.workbook.workbook.Workbook:
		return xlsx.get_sheet_names()
	return openpyxl.load_workbook(filename=xlsx, read_only=True).get_sheet_names()


def gen_linhas_xlsx(arquivo_xlsx, header=False, planilha=None):
	wb = openpyxl.load_workbook(filename=arquivo_xlsx, read_only=True)
	planilhas = [planilha] if planilha!=None else get_nome_planilhas(wb)
	for plan in planilhas:
		ws = wb[plan]
		for row in ws.rows:
			if header:
				header = False
			else: yield [cell.value for cell in row]
