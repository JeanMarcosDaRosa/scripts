import sublime, sublime_plugin
import datetime

class AtividadeCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		today = datetime.date.today()
		pos = self.view.sel()[0].begin()
		self.view.insert(edit, self.view.sel()[0].begin(), today.strftime('%d/%m/%Y')+"\t-\t - 1h")
		self.view.sel().clear()
		self.view.sel().add(sublime.Region(pos+13))
