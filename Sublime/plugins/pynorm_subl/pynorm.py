import sublime, sublime_plugin
import os 

class PynormCommand(sublime_plugin.TextCommand):

	def run(self, edit):
		nome_arq = self.view.file_name()
		if nome_arq.endswith('.py'):
			settings = sublime.load_settings('PyNorm.sublime-settings')
			texto = self.view.substr(sublime.Region(0, self.view.size()))
			norm = Normalizer(texto, c_ident=settings.get('spaces_identation', 4), clear_spaces=settings.get('clear_spaces', True))
			conteudo = norm.extract()
			if(settings.get('open_in_new_tab', True)):
				tab = self.view.window().new_file()
				tab.set_name(os.path.basename(nome_arq)+" - Normalized")
				tab.insert(edit, 0, str(conteudo))
				tab.set_syntax_file('Packages/Python/Python.tmLanguage')
			else:
				self.view.erase(edit, sublime.Region(0, self.view.size()))
				self.view.insert(edit, 0, str(conteudo))


class Line():
	"""
	Class reference line of file source
	this content an id, an level, start position (of first character of line),
	and the content of line
	"""
	def __init__(self, id_line, level, start, content):
		self.id = id_line
		self.level = level
		self.start = start
		self.content = content
		self.insert_space = False
		self.blank = ((content==None) or (content.strip()==""))


class Normalizer():

	def __init__(self, texto, c_ident=4, clear_spaces=True):
		self.texto = texto
		self.c_ident = c_ident
		self.clear_spaces = clear_spaces

	def extract(self):
		before_ident = 0
		level = 0
		line_id = 0
		lines = [x for x in self.texto.split('\n')]
		position_lines = []
		line_to_clear = None
		for content in lines:
			ident = self.space_begin_content(content)
			if ident != before_ident and len(content.strip())>0:
				if ident > before_ident:
					level += 1
				else:
					for l in reversed(position_lines):
						if not l.blank and ident == l.start:
							level = l.level
							break
				before_ident = ident
			line_id+=1
			new_line = Line(line_id, level, ident, content)
			position_lines.append(new_line)
			if not new_line.blank:
				self.check_insert_space(new_line, line_to_clear)
				line_to_clear = new_line
		return self.normalize_file(position_lines)

	def normalize_file(self, position_lines):
		out = ""
		for line in position_lines:
			outLine = line.content
			if line.level > 0:
				if not line.blank:
					outLine = self.normalize_line(line.content, line.level)
			if self.clear_spaces:
				if not line.blank:
					if line.insert_space:
						out += outLine + "\n\n"
					else:
						out += outLine + "\n"
			else:
				out += outLine + "\n"
		return out

	def normalize_line(self, str_x, breaks):
		if str_x != None:
			str_x = str_x.strip()
			for i in range(self.c_ident * breaks):
				str_x = " " + str_x
		return str_x

	def space_begin_content(self, content):
		ident = 0
		for c in content:
			if c == ' ' or c == '\t':
				ident+=1
			else:
				break
		return ident

	def check_insert_space(self, new_line, line_to_clear):
		if self.clear_spaces and new_line != None and line_to_clear != None:
			aux = new_line.content.strip()
			if (aux.startswith("def") or aux.startswith("class")) or ( not aux.startswith("import") and \
				line_to_clear.content.strip().startswith("import")):
				line_to_clear.insert_space = True