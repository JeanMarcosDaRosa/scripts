import sublime, sublime_plugin, sys
from goslate import Goslate

class TraduzirCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		for s in self.view.sel():
			translated = Goslate().translate(self.view.substr(s), 'pt-BR')
			if translated!=None and translated!='':
				self.view.replace(edit, s, translated)