import base64 as b
import zlib

#encode
st = b'<tag>&lt;INFORMACAO</tag>'
str_gzip = zlib.compress(st)
enc = b.b64encode(str_gzip)
#decode
c = b.b64decode(enc)
decompressed_data = zlib.decompress(c)
print(decompressed_data)