import crypt, getpass, string, random

pw = getpass.getpass('[*] Senha: ')
salto = '$6$'

for i in range(8):
    salto += random.choice(string.letters + string.digits)

salto += '$'

print(crypt.crypt(pw, salto))