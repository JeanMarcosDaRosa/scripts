import sys
import io
import os.path
import hashlib

import pprp
import pprp.config

# Make the strings the right type for the current Python version.
def trans(text):
    return text.encode('ASCII') if sys.version_info[0] >= 3 else text

passphrase = trans('EkRtyg!@#$#%-906')
salt = trans('asd098d74j(89!@d')
key_size = 32
data = ("f@rroup1lhAX").encode('ASCII')

key = pprp.pbkdf2(passphrase, salt, key_size)
sg = pprp.data_source_gen(data)
eg = pprp.rjindael_encrypt_gen(key, sg)
print(eg)