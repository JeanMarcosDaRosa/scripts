from Crypto.Cipher import AES
import base64
from rijndael import rijndael

iv = "asd098d74j(89!@d"
key = "EkRtyg!@#$#%-906"
message = "f@rroup1lhAX    "

obj = AES.new(key, AES.MODE_CBC, iv)
ciphertext = obj.encrypt(message)
print(ciphertext)
enc = base64.b64encode(ciphertext)
print(enc)
dec = AES.new(key, AES.MODE_CBC, iv)
print(dec.decrypt(base64.b64decode(enc)))
