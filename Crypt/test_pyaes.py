import pyaes

# A 256 bit (32 byte) key
key = "EkRtyg!@#$#%-906"

# For some modes of operation we need a random initialization vector
# of 16 bytes
iv = "asd098d74j(89!@d"

aes = pyaes.AESModeOfOperationOFB(key, iv = iv)
plaintext = "TextMustBe16Byte"
ciphertext = aes.encrypt(plaintext)