from pyasn1.codec.der.decoder import decode
from pyasn1_modules import rfc2459

from OpenSSL.crypto import *
from base64 import b64encode
from pyasn1.type import namedtype
from pyasn1.type import univ
from pyasn1_modules import pem
from pyasn1_modules import rfc2459
import sys

p12 = load_pkcs12(open("C:\\tmp\\tn3_certificado_2016.pfx", 'rb').read(), 'f@rroup1lhAX')
cert, rest = decode(p12.get_certificate().get_subject().der(), asn1Spec=rfc2459.SubjectAltName())
print(cert.prettyPrint())

