from OpenSSL.crypto import *
from base64 import b64encode
from pyasn1.codec.der import decoder
from pyasn1.codec.der import encoder
from pyasn1.type import namedtype
from pyasn1.type import univ
from pyasn1_modules import pem
from pyasn1_modules import rfc2459

p12 = load_pkcs12(open("C:\\tmp\\tn3_certificado_2016.pfx", 'rb').read(), 'f@rroup1lhAX')

cert_str = dump_certificate(FILETYPE_PEM, p12.get_certificate())
open('cert.pem', 'wb').write(cert_str)
objCert = p12.get_certificate().get_subject()
substrate = pem.readPemFromFile(open('cert.pem'))
cert = decoder.decode(substrate, asn1Spec=rfc2459.SubjectAltName())

print(cert.prettyPrint())
'''
cert = cert['tbsCertificate'] # just get the core part of the certificate
subject = cert['subject']
rdnsequence = subject[0] # the subject is only composed by one component
for rdn in rdnsequence:
   oid, value = rdn[0]  # rdn only has 1 component: (object id, value) tuple
   print(oid, ':', str(value))
'''