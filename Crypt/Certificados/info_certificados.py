import re
import os
import sys
from pprint import pprint
from datetime import date, datetime
from os.path import isfile, join
from Crypto.Cipher import AES
from OpenSSL.crypto import *
from pyasn1.type import univ
from pyasn1_modules import rfc2459
from pyasn1.codec.ber import encoder, decoder



def getInfoCertificado(filename, password):
    try:
        p12 = load_pkcs12(open(filename, 'rb').read(), password)
        objCert = p12.get_certificate().get_subject()
        cur_date = datetime.utcnow()
        cert_nbefore = datetime.strptime((p12.get_certificate().get_notBefore()).decode('utf8'), '%Y%m%d%H%M%SZ')
        cert_nafter = datetime.strptime((p12.get_certificate().get_notAfter()).decode('utf8'), '%Y%m%d%H%M%SZ')
        expire_days = int((cert_nafter - cur_date).days)
        responsavel = None
        cnpj = None
        for i in range(0, p12.get_certificate().get_extension_count()):
            extension = p12.get_certificate().get_extension(i)
            try:
                names = decoder.decode(extension.get_data(), asn1Spec=rfc2459.SubjectAltName())[0]
                for name in names:
                    if type(name.getComponent()[0]) == type(univ.ObjectIdentifier()):
                        if str(name.getComponent()[0]) == '2.16.76.1.3.2':
                            responsavel = re.sub('[^0-9a-zA-Z]+', ' ', str(name.getComponent()[1])).strip()
                        if str(name.getComponent()[0]) == '2.16.76.1.3.3':
                            cnpj = re.sub('[^0-9a-zA-Z]+', ' ', str(name.getComponent()[1])).strip()
            except Exception as e:
            	pass

        return {
            "descricao": str(objCert.commonName).strip(),
            "cnpj": cnpj,
            "responsavel": responsavel,
            "criado em": cert_nbefore.strftime("%d-%m-%Y %H:%M") if cert_nbefore!=None else None,
            "vencimento": cert_nafter.strftime("%d-%m-%Y %H:%M"),
            "dias restante": expire_days,
            "expirado": "sim" if (cur_date > cert_nafter) else "nao",
            "componentes": dict((x, y) for x, y in objCert.get_components()),
            "contem chave": "sim" if (p12.get_privatekey()!=None) else "nao",
            "contem cadeia": "sim" if (p12.get_ca_certificates()!=None) else "nao",
        }

    except Exception as a:
    	print(a)
    	return None

if __name__=='__main__':
	if len(sys.argv)!=5:
		print("Use %s -a arquivo.pfx -s senha" % sys.argv[0])
		exit(0)
	arquivo = None
	senha = None
	for x in range(len(sys.argv)):
		if sys.argv[x]=='-a':
			arquivo = sys.argv[x+1]
		if sys.argv[x]=='-s':
			senha = sys.argv[x+1]
	if isfile(arquivo) and str(arquivo).endswith(".pfx"):
		dados = getInfoCertificado(arquivo, senha)
		if dados:
			print("Dados do certificado:\n")
			pprint(dados)
		else:
			print("Nao conseguiu capturar os dados, verifique a senha")
	else:
		print("Nao conseguiu encontrar/ler o arquivo %s" % arquivo)
	#python cert.py -a "C:\tmp\certificados\gayatri2016.pfx" -s 12345678