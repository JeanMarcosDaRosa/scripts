from OpenSSL.crypto import *


from os import listdir
from os.path import isfile, join
pathfile="C:\\tmp\\certificados"
for f in listdir(pathfile):
	filename = join(pathfile,f)
	if isfile(filename) and str(f).endswith(".pfx"):
		print("========================================================")
		print("FILE: %s" % filename)
		password = filename[filename.index('--')+2:(filename.index(".pfx"))]
		p12 = load_pkcs12(open(filename, 'rb').read(), password)
		objCert = p12.get_certificate().get_subject()
		print(objCert.get_components())
		print(objCert.commonName)
		print(p12.get_privatekey())
		print(p12.get_ca_certificates())
		if p12.get_ca_certificates() != None:
			print("-----------------------------------------------------")
			for c in p12.get_ca_certificates():
				objCert = c.get_certificate().get_subject()
				print(c.get_components())
				print(c.commonName)
		print("========================================================")