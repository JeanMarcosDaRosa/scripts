import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa.system_manager import *
import json
import sys

sysLocal = SystemManager('localhost:9160')
sysMind  = SystemManager('213.136.81.102:9160')
kL = sysLocal.get_keyspace_column_families('MINDNET')
kM = sysMind.get_keyspace_column_families('MINDNET')
poolMind  = ConnectionPool('MINDNET',['213.136.81.102:9160'],timeout=1000)
poolLocal = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)

def create_index():
	print 'Criando indices...'
	print 'creating into SEMANTIC_RELACTIONS...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS', 'obj_orig', 'BytesType', index_name='relaction_obj_orig')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS', 'obj_dest', 'BytesType', index_name='relaction_oobj_dest')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS', 'opcode', 'BytesType', index_name='relaction_opcode')
	
	print 'creating into SEMANTIC_OBJECT_DT3...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT3', 'LEV', 'BytesType', index_name='object_LEV3')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT3', 'datach', 'BytesType', index_name='datach_index3')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT3', 'topico', 'BytesType', index_name='topico_index3')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT3', 'object', 'BytesType', index_name='object_index3')
	
	print 'creating into to_posting2...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT3', 'id_post', 'AsciiType', index_name='to_posting2_69645f706f7374_idx')
	
	print 'creating into cache_products...'
	sysLocal.create_index('MINDNET', 'cache_products', 'INDEXED', 'AsciiType', index_name='cache_products_494e4445584544_idx')
	
	print 'creating into SEMANTIC_OBJECT_DT3_1_4...'
	sysLocal.create_index('MINDNET', 'cache_products', 'LEV', 'BytesType', index_name='object_LEV3_1_4')
	sysLocal.create_index('MINDNET', 'cache_products', 'datach', 'BytesType', index_name='datach_index3_1_4')
	sysLocal.create_index('MINDNET', 'cache_products', 'topico', 'BytesType', index_name='topico_index3_1_4')
	sysLocal.create_index('MINDNET', 'cache_products', 'object', 'BytesType', index_name='object_index3_1_4')
	
	print 'creating into web_cache1...'
	sysLocal.create_index('MINDNET', 'web_cache1', 'indexed', 'AsciiType', index_name='web_cache1_696e6465786564_idx')
	
	print 'creating into SEMANTIC_OBJECT...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT', 'objeto', 'BytesType', index_name='object_objeto')
	
	print 'creating into web_cache3...'
	sysLocal.create_index('MINDNET', 'web_cache3', 'indexed', 'AsciiType', index_name='web_cache3_696e6465786564_idx')
	
	print 'creating into SEMANTIC_OBJECT3_1_4...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT3_1_4', 'objeto', 'BytesType', index_name='object_objeto3_1_4')
	
	print 'creating into cache_links...'
	sysLocal.create_index('MINDNET', 'cache_links', 'INDEXED', 'AsciiType', index_name='cache_links_494e4445584544_idx')
	
	print 'creating into web_cache10...'
	sysLocal.create_index('MINDNET', 'web_cache10', 'doc_id', 'AsciiType', index_name='web_cache10_646f635f6964_idx')
	
	print 'creating into SEMANTIC_OBJECT_DT...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT', 'LEV', 'BytesType', index_name='object_LEV')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT', 'datach', 'BytesType', index_name='datach_index')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT', 'topico', 'BytesType', index_name='topico_index')
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT_DT', 'object', 'BytesType', index_name='object_index')
	
	print 'creating into web_cache15...'
	sysLocal.create_index('MINDNET', 'web_cache15', 'id_usr', 'AsciiType', index_name='web_cache15_69645f757372_idx')
	
	print 'creating into fuzzy_store...'
	sysLocal.create_index('MINDNET', 'fuzzy_store', 'layout_onto', 'BytesType', index_name='fuzzy_store_layout_onto')
	
	print 'creating into fcb_users1...'
	sysLocal.create_index('MINDNET', 'fcb_users1', 'indexed', 'BytesType', index_name='fcb_users1_696e6465786564_idx')
	
	print 'creating into SEMANTIC_RELACTIONS3...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3', 'obj_orig', 'BytesType', index_name='relaction3_obj_orig')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3', 'obj_dest', 'BytesType', index_name='relaction3_obj_dest')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3', 'opcode', 'BytesType', index_name='relaction3_opcode')
	
	print 'creating into SEMANTIC_OBJECT3...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_OBJECT3', 'objeto', 'BytesType', index_name='object_objeto3')
	
	print 'creating into SEMANTIC_RELACTIONS3_1_4...'
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3_1_4', 'obj_orig', 'BytesType', index_name='relaction3_1_4_obj_orig')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3_1_4', 'obj_dest', 'BytesType', index_name='relaction3_1_4_obj_dest')
	sysLocal.create_index('MINDNET', 'SEMANTIC_RELACTIONS3_1_4', 'opcode', 'BytesType', index_name='relaction3_1_4_opcode')

def drop_cf(list_cf):
	for cf in list_cf:
		cfT = ColumnFamily(poolLocal, cf)
		cfT.truncate()
		sysLocal.drop_column_family('MINDNET', cf)

def create_cf(list_cf):
	for cf in list_cf:
		sysLocal.create_column_family('MINDNET', cf, key_validation_class='BytesType')
	create_index()
	
def count_rows_default(conn, tbl):
	c = ColumnFamily(conn,  tbl)
	#for k, r in c.get_range():
	#	print k, r
	print len(dict(c.get_range()).keys())

def clone_objs(list_cf, list_cf_get=None, trunc=False):
	for cf in list_cf:
		if (list_cf_get==None or cf in list_cf_get):
			idx = 0
			cfL = ColumnFamily(poolLocal, cf)
			cfM = ColumnFamily(poolMind,  cf)
			print 'clonando ',cf, '...'
			if trunc: cfL.truncate()
			print len(dict(cf.get_range(column_count=1)).keys())
			for key, row in cfM.get_range(buffer_size=4096):
				cfL.insert(key, row)
				idx+=1
				if idx % 5 == 0:
					print '\rRegistro' , idx,
					sys.stdout.flush()	
			print '\n', cf, '- done. count:', idx
	
#drop_cf(kL)
#create_cf(kM)

#all
#list_get = ['SEMANTIC_RELACTIONS', 'SEMANTIC_OBJECT_DT3', 'to_posting3', 'to_posting2', 'cache_products', 'fz_store_defs', 'knowledge_manager', 'SEMANTIC_OBJECT_DT3_1_4', 'web_cache1', 'SEMANTIC_OBJECT', 'web_cache3', 'fz_store_refer', 'SEMANTIC_OBJECT3_1_4', 'cache_links', 'web_cache10', 'SEMANTIC_OBJECT_DT', 'web_cache15', 'fuzzy_store', 'to_posting', 'fcb_users1', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'SEMANTIC_RELACTIONS3', 'fz_arround_points', 'SEMANTIC_OBJECT3', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY', 'to_posting_perfil', 'SEMANTIC_RELACTIONS3_1_4']
#processed list
#list_get = ['SEMANTIC_RELACTIONS', 'fz_store_defs', 'knowledge_manager', 'SEMANTIC_OBJECT', 'SEMANTIC_OBJECT_DT', 'fuzzy_store', 'DATA_BEHAVIOUR_PY', 'fz_store_sufix', 'SEMANTIC_OBJECT3', 'fz_store_pref', 'DATA_BEHAVIOUR_CODE_PY', 'SEMANTIC_RELACTIONS3_1_4', 'web_cache15']
#not processed list
#list_get = ['SEMANTIC_OBJECT_DT3', 'SEMANTIC_OBJECT_DT3_1_4', 'fz_store_refer', 'SEMANTIC_OBJECT3_1_4', 'SEMANTIC_RELACTIONS3', 'fz_arround_points', 'to_posting3', 'to_posting2', 'cache_products', 'web_cache3', 'cache_links', 'web_cache10', 'to_posting', 'fcb_users1', 'to_posting_perfil']

#processed one per one

#list_get = ['SEMANTIC_RELACTIONS']
#list_get = ['fz_store_defs']
#list_get = ['knowledge_manager']
#list_get = ['SEMANTIC_OBJECT']
#list_get = ['SEMANTIC_OBJECT_DT']
#list_get = ['fuzzy_store']
#list_get = ['DATA_BEHAVIOUR_PY']
#list_get = ['fz_store_sufix']
#list_get = ['SEMANTIC_OBJECT3']
#list_get = ['fz_store_pref']
#list_get = ['DATA_BEHAVIOUR_CODE_PY']
#list_get = ['SEMANTIC_RELACTIONS3_1_4']
#list_get = ['web_cache15']
#---------------------------------------
#list_get = ['web_cache1'] <- importa do backup por ser muito grande
#list_get = ['fz_store_refer'] <- zerado
#list_get = ['to_posting'] <- zerado
#list_get = ['cache_links'] <- zerado
#list_get = ['web_cache3'] <- zerado
#list_get = ['fz_arround_points'] <- zerado
#list_get = ['to_posting_perfil'] <- zerado
#list_get = ['to_posting2'] <- zerado
#list_get = ['to_posting3'] <- zerado
#list_get = ['fcb_users1'] <- zerado
#list_get = ['web_cache10'] <- zerado
#list_get = ['cache_products'] <- zerado
#list_get = ['SEMANTIC_RELACTIONS3'] <- zerado
#---------------------------------------

#no processed one per one

list_get = ['SEMANTIC_OBJECT_DT3']
#list_get = ['SEMANTIC_OBJECT_DT3_1_4']
#list_get = ['SEMANTIC_OBJECT3_1_4']

clone_objs(kM, list_get, True)

#count_rows_default(poolMind, 'DATA_BEHAVIOUR_CODE_PY')
#count_rows_default(poolLocal, 'DATA_BEHAVIOUR_CODE_PY')

sysLocal.close()
sysMind.close()

print 'done.'
