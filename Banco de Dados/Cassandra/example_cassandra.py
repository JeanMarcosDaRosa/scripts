import sys
sys.path.append("/home/jean/Programacao/Python/componentes/pycassa")
sys.path.append("/home/jean/Programacao/Python/componentes")
import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily

pool = ConnectionPool('demo',['192.168.0.106:9160'],timeout=1000)
col_fam = ColumnFamily(pool, 'users')
 
for i in range(900001,1000000):
	col_fam.insert(str(i), {'full_name': 'Edson ' + str(i)})

print 'done'


'''SET users['jo']['full_name']='Robert Jones';
SET users['jo']['email']='bobjones@gmail.com';
SET users['jo']['state']='TX';
SET users['jo']['gender']='M';
SET users['jo']['birth_year']='1975';'''
