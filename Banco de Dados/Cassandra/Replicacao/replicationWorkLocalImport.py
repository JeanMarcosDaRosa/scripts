import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import json

poolMind = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)

#cache_products
def cache_products():
	print 'cache_products'
	cache_products = ColumnFamily(poolMind, 'cache_products')
	cache_products.truncate()
	arq = open('cache_products.bkp', 'r')

	linhas = arq.read()
	for s in str(linhas).split("\x03"):
		if s!='':
			c = s.split("\x02")
			row = {'lomadee_url':c[1], 'cod_p': c[2], 'INDEXED': c[3]}
			cache_products.insert(c[0],row)

	print 'cache_products - done'

#web_cache1
def web_cache1():
	print 'web_cache1'
	web_cache1 = ColumnFamily(poolMind, 'web_cache1')
	web_cache1.truncate()

	arq = open('web_cache1.bkp', 'r')

	linhas = arq.readline()
	for s in str(linhas).split("\x03"):
		if s!='':
			c = s.split("\x02")
			print len(c)
			if len(c) == 18:
				row = {'doc_id':c[1], 'id': c[2], 'id_usr': c[3], 'indexed': c[4], 'name_usr': c[5], 'pg': c[6], 'processed': c[7], 'purpose': c[8], 'story': c[9], 'temo':c[10], 'title': c[11], 'tp': c[12], 'tps': c[13], 'url': c[14], 'url_icon': c[15], 'url_picture': c[16], 'usr': c[17]}
				web_cache1.insert(c[0],row)

	print 'web_cache1 - done'

	
#to_posting2
def to_posting2():
	print 'to_posting2'
	
	to_posting2 = ColumnFamily(poolMind, 'to_posting2')
	to_posting2.truncate()

	arq = open('to_posting2.bkp', 'r')

	linhas = arq.read()
	for s in str(linhas).split("\x03"):
		if s!='':
			c = s.split("\x02")
			row = {'from_id':c[1], 'id_post': c[2], 'id_product': c[3], 'kidexpub': c[4], 'link_publish': c[5]}
			to_posting2.insert(c[0],row)

	print 'to_posting2 - done'
	
#to_posting_perfil
def to_posting_perfil():
	print 'to_posting_perfil'
	
	to_posting_perfil = ColumnFamily(poolMind, 'to_posting_perfil')
	to_posting_perfil.truncate()

	arq = open('to_posting_perfil.bkp', 'r')

	linhas = arq.read()
	for s in str(linhas).split("\x03"):
		if s!='':
			c = s.split("\x02")
			row = {'from_id':c[1], 'MODALIDADE': c[2], 'opcode': c[3], 'layer_from': c[4], 'DATA': c[5]}
			to_posting_perfil.insert(c[0],row)

	print 'to_posting_perfil - done'

print '----------------------- Import DATA ---------------'
print 'cache_products -> 1'
print 'web_cache1 -> 2'
print 'to_posting2 -> 3'
print 'to_posting_perfil -> 4'
print 'all -> 5'
print '-------------------'

ent = raw_input('Entre com as opcoes separadas por espaco: ')

t = ent.split(' ')

for g in t:
	if g == '1':
		cache_products()
	elif g == '2':
		web_cache1()
	elif g == '3':
		to_posting2()
	elif g == '4':
		to_posting_perfil()
	elif g == '5':
		cache_products()
		web_cache1()
		to_posting2()
		to_posting_perfil()
	else:
		print 'Opcao invalida!'


print '------- done ----'