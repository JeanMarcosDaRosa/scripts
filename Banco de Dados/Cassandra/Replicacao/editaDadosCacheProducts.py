import urllib
import urllib2
import pycassa
import xml.dom.minidom
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import json 

poolMind = ConnectionPool('MINDNET',['localhost:9160'],timeout=1000)
#poolMind = ConnectionPool('MINDNET',['213.136.81.102:9160'],timeout=1000)
#cache_products

def lomadeezar_links(links_tw):
 areturn=[]
 lnk=[]
 ind=1
 #if len(links_tw)==0: return []
 for l in links_tw:
   #l=urllib.quote(l)
   lnk.append(['link'+str(ind),l])
   ind+=1
 url='http://ws.buscape.com/service/createLinks/lomadee/564771466d477a4458664d3d/?'
 #url='http://sandbox.buscape.com/service/createLinks/lomadee/564771466d477a4458664d3d/?' 
 args = {}  
 print 'process links to lomadee:',len(links_tw) 
 args['sourceId']='28009381'
 for ul in lnk:
   args[ul[0]]=ul[1]
 dt=urllib.urlencode(args)    
 file = urllib2.urlopen(url,dt)
 data_Resk = file.read()
 #print data_Resk
 dom2 = xml.dom.minidom.parseString(data_Resk)
 nods=dom2.childNodes
 for n1 in nods:
  n2=n1.getElementsByTagName('lomadeeLinks')
  if n2 != None:
   found_elems=False
   found_elem1=False
   for n3 in n2:
    n4=n3.getElementsByTagName('lomadeeLink')
    for n5 in n4:
     args={}
     found_elems=True
     for n6 in n5.getElementsByTagName('originalLink'):
      args['originalLink']=n6.firstChild.data
     for n7 in n5.getElementsByTagName('redirectLink'): 
      args['redirectLink']= n7.firstChild.data
      found_elem1=True
     for n8 in n5.getElementsByTagName('code'): 
      args['code']= n8.firstChild.data
     for n9 in n5.getElementsByTagName('id'): 
      args['id']= n9.firstChild.data
     areturn.append(args)
   if not found_elems or not found_elem1:
     print 'Lomadee error(1):',data_Resk,'\n',dt 
 return  areturn

def mostra_cache_products():
	print 'cache_products'
	str_t=""
	cache_products_src = ColumnFamily(poolMind, 'cache_products')
	to_posting2_src = ColumnFamily(poolMind, 'to_posting2')
	produtos=[]
	for key, row in to_posting2_src.get_range():
		k=row['id_product'].decode('hex')
		if k in produtos: continue
		produtos.append(k)
		row=cache_products_src.get(k)
		key=k
		k=str(key)
		nl=k.encode('hex')
		if row['INDEXED'] != 'E':
			str_t+= str(key).replace("\n", "")+",link do produto,"+row['cod_p']+","+row['INDEXED']
		if len(row) == 6:
			#str_t+= ","+row['preco']+","+row['oferta']+","+row['descricao']+","+row['t_image']
			str_t+= ","+row['preco']+","+row['oferta']+","+row['descricao']+","+row['t_image']
		else:
			str_t+= ","+'preco'+","+'oferta'+","+'descricao'+","+'t_image'
		str_t+=","+nl+"\n"
	return str_t


def update(filename):
	cache_products_src = ColumnFamily(poolMind, 'cache_products')
	#cache_products_src.truncate()
	arq = open(filename, 'r')
	linhas = arq.read()
	links=[]
	for s in str(linhas).split("\n"):
		if s!='':
			c = s.split(",")
			links.append(c[1])
	links=lomadeezar_links(links)
	idx_c=0
	for s in str(linhas).split("\n"):
		if s!='':
			p=''
			o=''
			d=''
			i=''
			cod=''
			idx=''
			c = s.split(",")
			if len(c) == 9:
				cod=c[2]
				idx=c[3]
				p=c[4]
				o=c[5]
				d=c[6]
				i=c[7]
			row = {'lomadee_url':links[idx_c]['redirectLink'], 'cod_p': cod, 'INDEXED' : idx, 'preco': p, 'oferta': o, 'descricao': d, 't_image': i}
			cache_products_src.insert(c[8].decode('hex'),row)
			idx_c+=1
			#print row
	

def saveFile(filename, string):
	print 'saveFile -> FileName [',filename,']'
	file_ = open(filename, 'w')
	file_.write(string)
	file_.close()


print '-------------------------------'
print 'mostar cache_products -> 1'
print 'salvar lista cache_products em um arquivo -> 2'
print 'alterar web_cache1 -> 3'
print '-------------------------------'

ent = raw_input('Entre com as opcoes separadas por espaco: ')

t = ent.split(' ')

for g in t:
	if g == '1':
		print mostra_cache_products()
		print 'cache_products - done'
	elif g == '2':
		saveFile('cache_products.csv',mostra_cache_products());
		print 'cache_products.csv - salvo'
	elif g == '3':
		print 'Carregando arquivo cache_products.csv'
		update('cache_products.csv')
		print 'done.'
	else:
		print 'Opcao invalida!'


print '------- done ----'




