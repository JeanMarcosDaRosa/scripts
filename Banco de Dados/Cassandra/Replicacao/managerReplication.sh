echo
echo 'Importar -> 1'
echo 'Exportar -> 2'

read opcao

if [ $opcao == "1" ];then
	echo
	echo 'Importando dados'
	echo 'Arquivos *.bkp necessarios!'
	echo '----------------------------------'

	echo 'Dados compactados?'
	echo 'y/n'
	read compac

	if [ $compac == 'y' ];then
		echo 'Dados compactados divididos?'
		echo 'y/n'
		read spl
		if [ $spl == 'y' ];then
			cat xa* > dados.tar.bz2
		fi

		tar -jxvf dados.tar.bz2
	fi
	
	python replicationWorkLocalImport.py
	#rm *.bkp

else
	if [ $opcao == "2" ];then
		echo
		echo 'Exportando arquivos'
		echo '-------------------------------'

		# chamar o script em python
		python replicationWorkLocalExport.py

		# compactar todos os arquivos com extensao *.bkp
		tar cjvf dados.tar.bz2 *.bkp
		rm *.bkp
		# se tamanho do arquivo gerado > 254MB dividir em pactes de 254MB
		tmax=254 # tamanho maximo do arquivo compactado

		du -m dados.tar.bz2 > tam
		tam=$(awk '{ print $1 }' tam)
		rm tam
		echo 'Tamanho do arquivo gerado eh ', $tam, 'MB'
		
		if [ $tam -gt $tmax ];then
			echo 'Dividindo em varios arquivos'
			split -b 254m dados.tar.bz2
		fi
	else
		echo 'Opcao inválida'
	fi
fi


echo
echo 'End of execution of shell script'
