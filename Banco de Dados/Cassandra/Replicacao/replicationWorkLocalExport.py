import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
import json 

poolMind = ConnectionPool('MINDNET',['213.136.81.102:9160'],timeout=1000)

#cache_products
def cache_products():
	print 'cache_products'
	idx=0
	strl=""
	cache_products_src = ColumnFamily(poolMind, 'cache_products')
	for key, row in cache_products_src.get_range():
		idx+=1
		#Aqui vai um if se conter os campos tal entra
		if row['INDEXED'] != 'E':
			strl += str(key)+"\2"+row['lomadee_url']+"\2"+row['cod_p']+"\2"+row['INDEXED']+"\3"

		if idx % 1000 == 0: 
			saveFile('cache_products.bkp', strl)
			strl=""
	if strl!= "":
		saveFile('cache_products.bkp', strl)
		strl=""
	print 'cache_products - done'

#web_cache1
def web_cache1():
	print 'web_cache1'
	idx=0
	strl=""
	web_cache1_src = ColumnFamily(poolMind, 'web_cache1')
	for key, row in web_cache1_src.get_range():
		idx+=1
		strl += str(key)+"\2"+row['doc_id']+"\2"+row['id']+"\2"+row['id_usr']+"\2"+row['indexed']+"\2"+row['name_usr']+"\2"+row['pg']+"\2"+row['processed']+"\2"+row['purpose']+"\2"+row['story']+"\2"+row['termo']+"\2"+row['title']+"\2"+row['tp']+"\2"+row['tps']+"\2"+row['url']+"\2"+row['url_icon']+"\2"+row['url_picture']+"\2"+row['usr']+"\3"
		if idx % 1000 == 0: 
			saveFile('web_cache1.bkp', strl)
			strl=""
	if strl!= "":
		saveFile('web_cache1.bkp', strl)
		strl=""
	print 'web_cache1 - done'
	
#to_posting2
def to_posting2():
	print 'to_posting2'
	idx=0
	strl=""
	to_posting2_src = ColumnFamily(poolMind, 'to_posting2')
	for key, row in to_posting2_src.get_range():
		idx+=1
		strl += str(key)+"\2"+row['from_id']+"\2"+row['id_post']+"\2"+row['id_product']+"\2"+row['kidexpub']+"\2"+row['link_publish']+"\3"
		if idx % 1000 == 0:
			saveFile('to_posting2.bkp', strl)
			strl=""
	if strl!= "":
		saveFile('to_posting2.bkp', strl)
		strl=""
	print 'to_posting2 - done'
	
#to_posting_perfil
def to_posting_perfil():
	print 'to_posting_perfil'
	idx=0
	strl=""
	to_posting_perfil_src = ColumnFamily(poolMind, 'to_posting_perfil')
	for key, row in to_posting_perfil_src.get_range():
		idx+=1
		strl += str(key)+"\2"+row['from_id']+"\2"+row['MODALIDADE']+"\2"+row['opcode']+"\2"+row['layer_from']+"\2"+row['DATA']+"\3"
		if idx % 1000 == 0: 
			saveFile('to_posting_perfil.bkp', strl)
			strl=""
	if strl!= "":
		saveFile('to_posting_perfil.bkp', strl)
		strl=""
	print 'to_posting_perfil - done'

def saveFile(filename, string):
	print 'saveFile -> FileName [',filename,']'
	file_ = open(filename, 'a')
	file_.write(string)
	file_.close()


print '----------------------- Export DATA ---------------'
print 'cache_products -> 1'
print 'web_cache1 -> 2'
print 'to_posting2 -> 3'
print 'to_posting_perfil -> 4'
print 'all -> 5'
print '-------------------'

ent = raw_input('Entre com as opcoes separadas por espaco: ')

t = ent.split(' ')

for g in t:
	if g == '1':
		cache_products()
	elif g == '2':
		web_cache1()
	elif g == '3':
		to_posting2()
	elif g == '4':
		to_posting_perfil()
	elif g == '5':
		cache_products()
		web_cache1()
		to_posting2()
		to_posting_perfil()
	else:
		print 'Opcao invalida!'


print '------- done ----'




