
# Trefas ja feitas

#CRUD
#	Consult
#	Read >> Feito
# 	Update
#	Delete


import cql
from prettytable import PrettyTable as pt
from termcolor import colored
import os


con = cql.connect('localhost', 9160, 'aula', cql_version='3.0.0')


def exibeCds():
	x = con.cursor()
	x.execute('select id, ano, artista, gravadora, titulo from cds;')
	tableDiscos = pt([colored('ID','red'), colored('ANO Lancamento', 'red'), colored('Artista', 'red'), colored('Gravadora','red'), colored('Titulo','red')])
	for i in x:
		tableDiscos.add_row([i[0], i[1], i[2], i[3], i[4]])

	print tableDiscos

def listarMusicasDiscos(dados):
	tableMusicas = pt([colored('Artista', 'red'), colored('Titulo','red'), colored('Musicas', 'red'), colored('Ano','red'), colored('Gravadora','red')])
	listMusic = []
	if dados[2] != None:
		for m in dados[2]:
			listMusic.append(str(m+' '+dados[2][m]))

	tableMusicas.add_row([dados[0], dados[1], '\n'.join(listMusic), dados[3], dados[4]])

	print tableMusicas	


def listarMusicasColetanias(dados):
	tableMusicasPLVA = pt([colored('Titulo','red'),colored('Artistas','red'),colored('Musica','red'), colored('Ano','red'), colored('Gravadora','red')])
	listaM = []
	for m in dados[2]:
		listaM.append(m.split('|')[0]+' '+m.split('|')[1]+' >> '+dados[2][m])

	tableMusicasPLVA.add_row([dados[1], dados[0], '\n'.join(listaM), dados[3], dados[4]])
	print tableMusicasPLVA


def listarMusicas(cod_disco):
	c = con.cursor()
	c.execute('select artista, titulo, musicas, ano, gravadora from cds where id = '+str(cod_disco)+';')

	for dados in c:
		if dados[0] == 'Varios Artistas' or dados[0] == 'Play List':
			listarMusicasColetanias(dados)
		else:
			listarMusicasDiscos(dados)

def insertAlbum():
	print '\n\tInsercao de novo disco!\n'
	
	cod_disco = raw_input('ID: ')
	artista = raw_input('Artista: ')
	titulo = raw_input('Titulo: ')
	gravadora = raw_input('Gravadora: ')
	ano = raw_input('Ano: ')
	
	musicas = []


	while True:
		nome = raw_input('Nome Muscica: ')
		duracao = raw_input('Duracao: ')
		if nome == '' or duracao == '':
			print 'Nome ou Duracao invalidos'
			if raw_input('\t(S)air?').upper() == 'S':
				break
			continue

		temp = {nome:duracao}
		musicas.append(temp)
		if raw_input('\t(S)air?').upper() == 'S':
			break

	insert = 'insert into cds (id, ano, artista, gravadora, titulo) values('+cod_disco+', \''+ano+'\', \''+artista+'\', \''+gravadora+'\', \''+titulo+'\');'
	c = con.cursor()
	c.execute(insert)
	for m in musicas:
		update = 'update cds set musicas = musicas + '+str(m)+' where id = '+cod_disco+';'
		c.execute(update)

	print 'Done'

def insertVariosArtistasPlayList():
	print '\n\tInsercao de novo disco!\n'
	
	cod_disco = raw_input('ID: ')
	op = ''
	if raw_input('(V)arios Artistas | (P)lay Lista').upper() == 'V': 
		op = 'Varios Artistas'
	else:
		op = 'Play List'

	titulo = raw_input('Titulo: ')
	gravadora = raw_input('Gravadora: ')
	ano = raw_input('Ano: ')

	
	musicas = []


	while True:
		nome = raw_input('Nome Muscica: ')
		duracao = raw_input('Duracao: ')
		artista = raw_input('Artista: ')
		if nome == '' or duracao == '':
			print 'Nome ou Duracao invalidos'
			if raw_input('\t(S)air?').upper() == 'S':
				break
			continue

		temp = {nome+'|'+duracao:artista}
		musicas.append(temp)
		if raw_input('\t(S)air?').upper() == 'S':
			break

	insert = 'insert into cds (id, ano, artista, gravadora, titulo) values('+cod_disco+', \''+ano+'\', \''+op+'\', \''+gravadora+'\', \''+titulo+'\');'
	c = con.cursor()
	c.execute(insert)
	for m in musicas:
		update = 'update cds set musicas = musicas + '+str(m)+' where id = '+cod_disco+';'
		c.execute(update)

	print 'Done'
	

def inserirDisco():
	op = raw_input('\n\n(V)arios Artistas | (P)lay List | (A)lbum\n\tResp: ')

	if op.upper() == 'V':
		insertVariosArtistasPlayList()
	elif op.upper() == 'P':
		insertVariosArtistasPlayList()
	elif op.upper() == 'A':
		insertAlbum()


def deleteDisco(cod_disco):
	c = con.cursor()
	listarMusicas(cod_disco)
	op = raw_input('(D)eletear | (C)ancelar\nResp: ').upper()
	if op == 'D':
		try:
			c.execute('delete from cds where id = '+str(cod_disco)+';')
		except Exception, e:
			print 'deleteDisco\nErro [', e, ']'
	elif op == 'C':
		print 'Operacao cancelada pelo usuario'
	else:
		print 'Operacao Inavalida!'

	print 'done'

 

def updates(cod_disco):
	c = con.cursor()
#	c.execute('select id, ano, artista, gravadora, musicas, titulo from cds where id = '+str(cod_disco)+';')

	listarMusicas(cod_disco)

	op = raw_input('Modificar (A)rtista\nModificar A(N)o\nModificar (G)ravadora\nModificar (T)itulo\nModificar (M)usicas\n(C)ancelar\nResp: ')
	op = op.upper()
	if op == 'A':
		artista = raw_input('Novo nome: ')
		try:
			c.execute('update cds set artista = \''+artista+'\' where id = '+str(cod_disco)+';')
		except Exception, e:
			print 'updates\nErro [', e, ']'
	elif op == 'N':
		ano = raw_input('Digite ano: ')
		try:
			c.execute('update cds set ano = \''+ano+'\' where id = \''+str(cod_disco)+';')
		except Exception, e:
			print 'updates\nErro [', e, ']'

	elif op == 'G':
		gravadora = raw_input('Digite a gravadora: ')
		try:
			c.execute('update cds set gravadora = \''+gravadora+'\' where id = '+str(cod_disco)+';')
		except Exception, e:
			print 'updates\nErro[', e, ']'
	elif op == 'T':
		titulo = raw_input('Titulo: ')
		try:
			c.execute('update cds set titulo = \''+titulo+'\' where id = '+str(cod_disco)+';')
		except Exception, e:
			print 'updates\nErro [', e, ']'
	elif op == 'M':
		print 'Modificando Musias'
		opM = raw_input('(D)eletar | (M)odificar | (A)dicionar | (C)ancelar').upper()
		
		if opM == 'D' and opM == 'M':
			while True:
				if opM == 'M' or opM == 'D':
					print colored('Atencao, caso seja uma \"Play List\" ou \"Varios Artistas\" no proximo campo adicione o dados seguindo formato >> nomeMusica|tempo <<', 'yellow' )
					musica = raw_input(colored('Nome da musica exatamente igual ao disco: ', 'blue'))
				if opM == 'D':
					try:
						c.execute('delete musicas [\''+str(musica)+'\'] from cds where id = '+str(cod_disco)+';')
						print 'Done'
					except Exception, e:
						print 'updates\nErro [', e, ']'
				elif opM == 'M':
					if '|' in musica:
						print 'Modificando musica de coletania'
						novaMusica = raw_input('Novo nome para a musica: ')
						novoDuracao = raw_input('Informe a duracao: ')
						novoArtista = raw_input('Informe o artista: ')
						try:
							novaMusica = novaMusica+'|'+novoDuracao
							c.execute('delete musicas [\''+str(musica)+'\'] from cds where id = '+str(cod_disco)+';')
							c.execute('update cds set musicas = musicas + {\''+novaMusica+'\':\''+novoArtista+'\'} where id = '+str(cod_disco)+';')
						except Exception, e:
							raise e
					else:
						novaMusica = raw_input('Novo nome para a musica: ')
						novoDuracao = raw_input('Informe a duracao: ')
						try:
							c.execute('delete musicas [\''+str(musica)+'\'] from cds where id = '+str(cod_disco)+';')
							c.execute('update cds set musicas = musicas + {\''+novaMusica+'\':\''+novoDuracao+'\'} where id = '+str(cod_disco)+';')
						except Exception, e:
							print 'updates\nErro ao modificar musica de album [', e, ']'

				if raw_input('(S)air?\nResp: ').upper() == 'S':
					break

		if opM == 'A':
			while True:
				novaMusica = raw_input('Novo nome para a musica: ')
				novoDuracao = raw_input('Informe a duracao: ')
				try:
					c.execute('update cds set musicas = musicas + {\''+novaMusica+'\':\''+novoDuracao+'\'} where id = '+str(cod_disco)+';')
				except Exception, e:
					print 'updates\nErro ao modificar musica de album [', e, ']'

				if raw_input('(S)air?').upper() == 'S':
					break
	elif op =='C':
		print 'Cancelado pelo Usuario!'
	else:
		print 'Operacao invalida!'



os.system('figlet Biblioteca de CDS')

while True:
	print '(L)istar cds'
	print '(A)dicionar Disco'
	print '(D)eleter cd'
	print 'At(U)alizar'
	print 'Listar (M)usicas do disco'
	print '(S)air'
	
	op = raw_input('Informe uma das opcoes acima: ').upper()
	

	if op == 'L':
		exibeCds()
	elif op == 'A':
		inserirDisco()
	elif op == 'D':
		deleteDisco(raw_input('ID: '))
	elif op == 'U':
		updates(raw_input('ID: '))
	elif op == 'S':
		break
	elif op == 'M':
		listarMusicas(raw_input('ID: '))






