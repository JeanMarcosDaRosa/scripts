from sqlalchemy import *
from fuzzywuzzy import fuzz
import re

metadata = MetaData(create_engine('mysql://tn3:tn3m@ster@192.168.0.20/cep'))
metadata.bind.echo = True

ufs = {
	'11':'RO','12':'AC','13':'AM','14':'RR','15':'PA','16':'AP','17':'TO','21':'MA','22':'PI','23':'CE',
	'24':'RN','25':'PB','26':'PE','27':'AL','28':'SE','29':'BA','31':'MG','32':'ES','33':'RJ','35':'SP',
	'41':'PR','42':'SC','43':'RS','50':'MS','51':'MT','52':'GO','53':'DF'
}

tabela_ibge = Table('tn3_ibge', metadata, autoload=True)
tabela_cep = Table('log_localidade', metadata, autoload=True)

cep, ibge = set(), set()
mapaCep, mapaIbge = {}, {}
listaNaoEncontrados = []

for x in tabela_ibge.select().execute().fetchall():
	ibge.add((x[2], ufs[str(x[3])]))
	mapaIbge[(x[2], ufs[str(x[3])])] = x[1] #codigo ibge

for x in tabela_cep.select().execute().fetchall():
	if x[6]=='M':
		cep.add((x[2], x[4]))
		mapaCep[(x[2], x[4])] = x[0] #codigo da cidade

print("Quantidade de cidades em IBGE: %d" % len(ibge))
print("Quantidade de cidades em CEP: %d" % len(cep))
print("Diferenca: %d" % len(cep.difference(ibge)))

listaBuscar = sorted(ibge.difference(cep))
merge = dict((mapaCep[x], mapaIbge[x]) for x in cep.intersection(ibge))

for x in sorted(cep.difference(ibge)):
	encontrou, mun_rank, mun_nome = None, 0, None
	for y in listaBuscar:
		pert = fuzz.QRatio(x[0], y[0])
		if x[1] == y[1] and pert >= 80:
			encontrou = y
			merge[mapaCep[x]] = mapaIbge[y]
			break
		elif pert > mun_rank:
			mun_rank, mun_nome = pert, y
	if encontrou:
		listaBuscar.remove(encontrou)
	else:
		listaNaoEncontrados.append(((x[0], x[1]), mun_rank, mun_nome))

print("Resolvidos com Fuzzy: %d" % (len(cep.difference(ibge))-len(listaNaoEncontrados)))
print("---------------------------------------------------------------")
print("Sobrou da tabela de IBGE:")
for nf in listaBuscar: print("\t"+nf[0]+" - "+nf[1])
print("Sobrou da tabela do CEPs:")
for nf in listaNaoEncontrados: print("\t"+"Nao encontrou %s ! maior pertinencia: %d com '%s'" % (nf[0][0]+" - "+nf[0][1], nf[1], nf[2]))
print("---------------------------------------------------------------")
for c, i in merge.items(): print("UPDATE CIOT_LOCALIDADE SET LOC_IBGE = '%s' WHERE LOC_CODIGO = %s;" % (i, c))