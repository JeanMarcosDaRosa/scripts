 #coding utf-8

import csv
import openpyxl


def gen_linhas_csv(arquivo_csv, header=False):
	inp = csv.reader(open(arquivo_csv, 'r'))
	for i in inp:
		if header:
			header = False
		else: yield i


def importar_xlsx(arquivo_xlsx, header=False, planilha=None):
	return [x for x in gen_linhas_xlsx(arquivo_xlsx, header=header, planilha=planilha)]


def importar_csv(arquivo_xlsx, header=False):
	return [x for x in gen_linhas_csv(arquivo_xlsx, header=header)]


def get_nome_planilhas(xlsx):
	if type(xlsx) == openpyxl.workbook.workbook.Workbook:
		return xlsx.get_sheet_names()
	return openpyxl.load_workbook(filename=arquivo_xlsx, read_only=True).get_sheet_names()


def gen_linhas_xlsx(arquivo_xlsx, header=False, planilha=None):
	wb = openpyxl.load_workbook(filename=arquivo_xlsx, read_only=True)
	planilhas = [planilha] if planilha!=None else get_nome_planilhas(wb)
	for plan in planilhas:
		ws = wb[plan]
		for row in ws.rows:
			if header:
				header = False
			else: yield [cell.value for cell in row]


if __name__=='__main__':
	#registros = importar_xlsx('participantes.xlsx', header=True)
	#print("Importacao Concluida!!! Total de %s registros." % len(registros))
	for x in gen_linhas_xlsx('participantes.xlsx', header=True, planilha='participantes'):
		print(x)
		break