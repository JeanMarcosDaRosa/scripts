#coding utf-8

import csv, dbf
from pymongo import MongoClient
from datetime import datetime


def grava_banco(database_mng, collection_mng, mapa, empresa):
	db = MongoClient()[database_mng]
	col = db[collection_mng]
	for k, v in mapa:
		col.insert_one({'id_empresa': empresa, 'nome': v[0], 'email': v[10], 'estado_endr': v[5], 'cidade_endr': v[3], 'bairro_endr': '', \
			'rua_endr': v[2]+', CEP: '+v[4], 'numero_endr': '', 'telefone1': v[8], 'telefone2': '', 'telefone3': '', 'celular1': v[9], 'op_celular1': '', \
			'celular2': '', 'op_celular2': '', 'celular3': '', 'op_celular3': '', 'detalhes': 'CNPJ/CPF: '+v[6]+', IE: '+v[7], 'data_cad': datetime.now().strftime('%Y/%d/%m %H:%M:%S')})


def gravar_novo_csv(mapa, arquivo='result.csv'):
	dados = []
	for k, v in mapa:
		dados.append(tuple(v))
	w = csv.writer(open(arquivo, 'w', newline=''), quoting=csv.QUOTE_ALL)
	for d in dados:
		try:
			w.writerow(tuple(d))
		except Exception as e:
			print(e)


def importar_cliente_csv(arquivo_csv, header, database_mng, collection_mng, empresa, mapa=None, gera_novo_csv=False, novo_csv='result.csv', grava_mng=True, show_logs=False):
	inp = csv.reader(open(arquivo_csv, 'r'))
	count = 0
	if mapa==None:
		mapa = {}
	for i in inp:
		if header and count==0:
			header = False
		elif i[0].strip().upper() not in mapa:
			if show_logs:
				print(i[:-3])
			mapa[i[0].strip().upper()] = i
			count+=1
	if show_logs:
		print("%s registros no arquivo csv." % count)
	mapa = sorted(mapa.items(), key=lambda x: x[1][0])
	if gera_novo_csv:
		gravar_novo_csv(mapa, novo_csv)
	if grava_mng:
		grava_banco(database_mng, collection_mng, mapa, empresa)


def importar_cliente_dbf(arquivo_dbf, database_mng, collection_mng, empresa, mapa=None, gera_novo_csv=False, novo_csv='result.csv', grava_mng=True, show_logs=False):
	inp = dbf.Table(arquivo_dbf, codepage='cp437')
	inp.open()
	#"Nome Fantasia","Razao Social","Endereco","Cidade","CEP","U.F.","C.N.P.J./C.P.F.","Insc. Est.","Telefone","Celular","e-mail"
	count = 0
	if mapa==None:
		mapa = {}
	for i in inp:
		try:
			k = [str(i.cli_nome).strip(), "", str(i.cli_ender).strip(), str(i.cli_cid).strip(), "", str(i.cli_uf).strip(), \
				str(i.cli_cnpj).strip(), str(i.cli_insc).strip(), str(i.cli_fone1).strip(), str(i.cli_celula).strip(), \
				str(i.cli_email).strip()]
			if str(i.cli_nome).strip().upper() not in mapa:
				if show_logs:
					print(k)
				mapa[str(i.cli_nome).strip().upper()] = k
				count+=1
		except:
			pass
	if show_logs:
		print("%s registros no arquivo dbf." % count)
	mapa = sorted(mapa.items(), key=lambda x: x[1][0])
	if gera_novo_csv:
		gravar_novo_csv(mapa, novo_csv)
	if grava_mng:
		grava_banco(database_mng, collection_mng, mapa, empresa)


def importar_fornece_dbf(arquivo_dbf, database_mng, collection_mng, empresa, mapa=None, gera_novo_csv=False, novo_csv='result.csv', grava_mng=True, show_logs=False):
	inp = dbf.Table(arquivo_dbf, codepage='cp437')
	inp.open()
	count = 0
	if mapa==None:
		mapa = {}
	for i in inp:
		try:
			k = [str(i.for_razao).strip(), "", str(i.for_ender).strip(), str(i.for_cid).strip(), "", str(i.for_uf).strip(), \
				str(i.for_cnpj).strip(), str(i.for_insc).strip(), str(i.for_fone1).strip(), str(i.for_celula).strip(), \
				str(i.for_email).strip()]
			if str(i.for_razao).strip().upper() not in mapa:
				if show_logs:
					print(k)
				mapa[str(i.for_razao).strip().upper()] = k
				count+=1
		except:
			pass
	if show_logs:
		print("%s registros no arquivo dbf." % count)
	mapa = sorted(mapa.items(), key=lambda x: x[1][0])
	if gera_novo_csv:
		gravar_novo_csv(mapa, novo_csv)
	if grava_mng:
		grava_banco(database_mng, collection_mng, mapa, empresa)


mapa = {}
importar_cliente_csv('entrada.csv', True, 'ecommerce', 'clientes', 'estruturafitness', mapa=mapa, gera_novo_csv=False, grava_mng=False, show_logs=True)
importar_cliente_dbf('bancodbf/CLI00.DBF', 'ecommerce', 'clientes', 'estruturafitness', mapa=mapa, gera_novo_csv=False, grava_mng=True, show_logs=True)
importar_fornece_dbf('bancodbf/FOR00.DBF', 'ecommerce', 'fornecedores', 'estruturafitness', gera_novo_csv=False, novo_csv='result_for.csv', grava_mng=True, show_logs=True)
print("Importacao Concluida!!! Total de %s registros." % len(mapa))